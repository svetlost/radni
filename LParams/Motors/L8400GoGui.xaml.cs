﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using StClient.Wagons;

namespace StClient
{
    /// <summary>
    /// Interaction logic for Vagoni003.xaml
    /// </summary>
    public partial class L8400GoGui : UserControl
    {
        Commands commands = new Commands();
        //VagonLogic003 vl;

        //public VagonGoGui(VagonLogic003 vagon)
        //{
        //    InitializeComponent();

        //    this.DataContext = vl = vagon;

        //}
        public L8400GoGui()
        {
            InitializeComponent();

            //20140512radovic
            //TimedAction.ExecuteWithDelay(new Action(delegate { this.DataContext = Sys.vagoniRxTx; this.SV.Text = "100"; }), TimeSpan.FromSeconds(3));

        }

        private void bRst_Click(object sender, RoutedEventArgs e)
        {
            foreach (var asfda in Sys.StaticL8400List) asfda.Group = false;
        }

        private void bDown_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            long i = 0;
            foreach (var asfda in Sys.StaticL8400List.Where(dsf => dsf.Group)) i += asfda.Down;

            UdpTx.UniqueInstance.SendHoistPDO(i);
        }

        private void bUp_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            long i = 0;
            foreach (var asfda in Sys.StaticL8400List.Where(dsf => dsf.Group)) i += asfda.Up;

            UdpTx.UniqueInstance.SendHoistPDO(i);
        }

        private void b_MouseUp(object sender, MouseButtonEventArgs e)
        {
            UdpTx.UniqueInstance.SendHoistPDO(0);
        }

    }
}
