﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Threading;

namespace StClient
{
    public class KompenzacioneRxTx
    {
        #region UniqueInstance
        class KompenzacioneRxTxCreator
        {
            static KompenzacioneRxTxCreator() { }
            internal static readonly KompenzacioneRxTx uniqueInstance = new KompenzacioneRxTx();
        }
        public static KompenzacioneRxTx UniqueInstance
        {
            get { return KompenzacioneRxTxCreator.uniqueInstance; }
        }


        #endregion

        //public int KompenzacioneCan = 2;
        public KompenzacioneRxTx()
        {
            KompList.Add(KP_L1 = new KompenzacionaLogic003(2, "KP LEFT 1"));
            KompList.Add(KP_L2 = new KompenzacionaLogic003(3, "KP LEFT 2"));
            KompList.Add(KP_L3 = new KompenzacionaLogic003(4, "KP LEFT 3"));
            KompList.Add(KP_R1 = new KompenzacionaLogic003(5, "KP RIGHT 1"));
            KompList.Add(KP_R2 = new KompenzacionaLogic003(6, "KP RIGHT 2"));
            KompList.Add(KP_R3 = new KompenzacionaLogic003(7, "KP RIGHT 3"));
            KompList.Add(KP_ROT = new KompenzacionaLogic003(8, "KP ROT"));

            timer.Interval = TimeSpan.FromMilliseconds(300);
            timer.Tick += timer_Tick;
            
            //20140512radovic
            //timer.Start();
        }

        public KompenzacionaLogic003 KP_L1, KP_L2, KP_L3, KP_R1, KP_R2, KP_R3, KP_ROT;
        public static List<KompenzacionaLogic003> KompList = new List<KompenzacionaLogic003>();
        KompenzacionaLogic003 tempKomp;

        DispatcherTimer timer = new DispatcherTimer();
        void timer_Tick(object sender, EventArgs e)
        {
            foreach (var komp in KompList) ProcessSM(komp);
            KompTx();
        }

        public static void TotalStop() { foreach (var t in KompList) t.TotalStop(); }

        SM_State oldState;
        public void ProcessSM(KompenzacionaLogic003 KompLogic)
        {
            switch (KompLogic.State)
            {
                case (SM_State.SM_Idle):
                    KompLogic.ky3 = false;
                    KompLogic.relayDown = KompLogic.relayUp = false;
                    //todo 20140217 vrati lineg i limpos kao uslov za komandu
                    if (KompLogic.UP && !KompLogic.logic.LimPos) KompLogic.State = SM_State.SM_Up;
                    else if (KompLogic.DOWN && !KompLogic.logic.LimNeg) KompLogic.State = SM_State.SM_Down;
                    //if (KompLogic.UP) KompLogic.State = SM_State.SM_Up;
                    //else if (KompLogic.DOWN) KompLogic.State = SM_State.SM_Down;
                    else KompLogic.State = SM_State.SM_Idle;
                    break;
                case (SM_State.SM_Up):
                    KompLogic.ky3 = true;
                    KompLogic.relayDown = false;
                    KompLogic.relayUp = true;
                    if (KompLogic.logic.LimPos || KompLogic.STOP) KompLogic.State = SM_State.SM_Stop;
                    else KompLogic.State = SM_State.SM_Up;
                    break;
                case (SM_State.SM_Down):
                    KompLogic.ky3 = true;
                    KompLogic.relayDown = true;
                    KompLogic.relayUp = false;
                    if (KompLogic.logic.LimNeg || KompLogic.STOP) KompLogic.State = SM_State.SM_Stop;
                    else KompLogic.State = SM_State.SM_Down;
                    break;
                case (SM_State.SM_Stop):
                    KompLogic.ky3 = true;
                    KompLogic.relayDown = false;
                    KompLogic.relayUp = false;
                    KompLogic.State = SM_State.SM_Idle;
                    break;
            }
            //if (KompLogic.oldState != KompLogic.State) KompTx();
            //KompLogic.oldState = KompLogic.State;
            //if (KompLogic.logic.Addr == 2) Debug.WriteLine("state={0}", KompLogic.State);

            if (KompLogic.Lock && Sys.LocksEnabled && (Sys.LocksOverrideEnabled && !Sys.lockMotors.OverrideActive))
                KompLogic.enableUpDown = false;
            else KompLogic.enableUpDown = true;
        }

        //byte[] data = new byte[CanStructure.CanOverUdpMessageLength];
        public void Komp8400Rx(byte[] dataReceived, int CanAddress)
        {
            tempKomp = null;
            tempKomp = KompList.Find(q => q.logic.Addr == CanAddress);
            if (tempKomp == null) return;

            tempKomp.logic.status1 = (ushort)(dataReceived[0] + 256 * dataReceived[1]);
            //tempKomp.logic.CV = BitConverter.ToInt16(new byte[] { dataReceived[2], dataReceived[3] }, 0) / 163.84 / 60 * 100;
            tempKomp.logic.CV = BitConverter.ToInt16(new byte[] { dataReceived[2], dataReceived[3] }, 0) * 0.01017;//60% brzine prikazujemo kao 100%
            tempKomp.logic.Trip = Sys.getBitOfInt(tempKomp.logic.status1, 2);
            tempKomp.logic.Brake = !Sys.getBitOfInt(tempKomp.logic.status1, 3);
            tempKomp.logic.Inh28 = Sys.getBitOfInt(tempKomp.logic.status1, 4);
            //tempKomp.logic.Moving = Sys.getBitOfInt(tempKomp.logic.status1, 10);
            tempKomp.logic.CanResponseCount++;
            tempKomp.logic.Moving = Math.Abs(tempKomp.logic.CV) > 0.5;
        }
        public void KompDioRx(byte[] dataReceived, int CanAddress)
        {
            if (CanAddress == 16)
            {
                KP_L1.logic.LimNeg = Sys.getBitOfInt(dataReceived[0], 0);
                KP_L3.logic.LimNeg = Sys.getBitOfInt(dataReceived[0], 1);
                KP_L1.logic.LimPos = Sys.getBitOfInt(dataReceived[0], 2);
                KP_L3.logic.LimPos = Sys.getBitOfInt(dataReceived[0], 3);
                KP_L2.logic.LimNeg = Sys.getBitOfInt(dataReceived[0], 4);
                KP_R3.logic.LimNeg = Sys.getBitOfInt(dataReceived[0], 5);
                KP_L2.logic.LimPos = Sys.getBitOfInt(dataReceived[0], 6);
                KP_R3.logic.LimPos = Sys.getBitOfInt(dataReceived[0], 7);
            }
            else if (CanAddress == 20)
            {
                KP_R1.logic.LimNeg = Sys.getBitOfInt(dataReceived[0], 0);
                KP_R1.logic.LimPos = Sys.getBitOfInt(dataReceived[0], 2);
                KP_R2.logic.LimNeg = Sys.getBitOfInt(dataReceived[0], 4);
                KP_R2.logic.LimPos = Sys.getBitOfInt(dataReceived[0], 6);
            }
            else if (CanAddress == 32)
            {
                KP_ROT.logic.LimNeg = Sys.getBitOfInt(dataReceived[0], 0);
                KP_ROT.logic.LimPos = Sys.getBitOfInt(dataReceived[0], 1);
            }
        }

        int dataToSend_Dio16, dataToSend_Dio20, dataToSend_Dio32;
        int dataToSend_Sbc18, dataToSend_Sbc24, dataToSend_Sbc34;
        public void KompTx()
        {
            dataToSend_Dio16 = dataToSend_Dio20 = dataToSend_Dio32 = 0;
            dataToSend_Sbc18 = dataToSend_Sbc24 = dataToSend_Sbc34 = 0;
            //canin1RX @DIO16=528=512+16, @SBC18=530, @DIO20=532, @SBC24=536, @DIO32=544, @SBC34=546

            dataToSend_Dio16 = Sys.SetBitOfInt(dataToSend_Dio16, 0, KP_L1.relayUp);
            dataToSend_Dio16 = Sys.SetBitOfInt(dataToSend_Dio16, 1, KP_L3.relayUp);
            dataToSend_Dio16 = Sys.SetBitOfInt(dataToSend_Dio16, 2, KP_L1.relayDown);
            dataToSend_Dio16 = Sys.SetBitOfInt(dataToSend_Dio16, 3, KP_L3.relayDown);
            dataToSend_Dio16 = Sys.SetBitOfInt(dataToSend_Dio16, 4, KP_L2.relayUp);
            dataToSend_Dio16 = Sys.SetBitOfInt(dataToSend_Dio16, 5, KP_R3.relayUp);
            dataToSend_Dio16 = Sys.SetBitOfInt(dataToSend_Dio16, 6, KP_L2.relayDown);
            dataToSend_Dio16 = Sys.SetBitOfInt(dataToSend_Dio16, 7, KP_R3.relayDown);
            dataToSend_Sbc18 = Sys.SetBitOfInt(dataToSend_Sbc18, 0, KP_L1.ky3);
            dataToSend_Sbc18 = Sys.SetBitOfInt(dataToSend_Sbc18, 2, KP_L2.ky3);
            dataToSend_Sbc18 = Sys.SetBitOfInt(dataToSend_Sbc18, 4, KP_L3.ky3);
            dataToSend_Sbc18 = Sys.SetBitOfInt(dataToSend_Sbc18, 6, KP_R3.ky3);
            UdpTx.UniqueInstance.SendPDORaw(528, Sys._KompenzacioneCanNetworkId, new byte[8] { (byte)dataToSend_Dio16, 0, 0, 0, 0, 0, 0, 0 }, 0, 1);
            UdpTx.UniqueInstance.SendPDORaw(530, Sys._KompenzacioneCanNetworkId, new byte[8] { (byte)dataToSend_Sbc18, 0, 0, 0, 0, 0, 0, 0 }, 0, 1);
            //Debug.WriteLine("===dio16={0:X}", dataToSend_Dio16);
            //Debug.WriteLine("===dio18={0:X}", dataToSend_Sbc18);

            dataToSend_Dio20 = Sys.SetBitOfInt(dataToSend_Dio20, 0, KP_R1.relayDown);
            dataToSend_Dio20 = Sys.SetBitOfInt(dataToSend_Dio20, 2, KP_R1.relayUp);
            dataToSend_Dio20 = Sys.SetBitOfInt(dataToSend_Dio20, 4, KP_R2.relayDown);
            dataToSend_Dio20 = Sys.SetBitOfInt(dataToSend_Dio20, 6, KP_R2.relayUp);
            dataToSend_Sbc24 = Sys.SetBitOfInt(dataToSend_Sbc24, 0, KP_R1.ky3);
            dataToSend_Sbc24 = Sys.SetBitOfInt(dataToSend_Sbc24, 1, KP_R2.ky3);
            UdpTx.UniqueInstance.SendPDORaw(532, Sys._KompenzacioneCanNetworkId, new byte[8] { (byte)dataToSend_Dio20, 0, 0, 0, 0, 0, 0, 0 }, 0, 1);
            UdpTx.UniqueInstance.SendPDORaw(536, Sys._KompenzacioneCanNetworkId, new byte[8] { (byte)dataToSend_Sbc24, 0, 0, 0, 0, 0, 0, 0 }, 0, 1);
            //Debug.WriteLine("===dio20={0:X}", dataToSend_Dio20);
            //Debug.WriteLine("===dio24={0:X}", dataToSend_Sbc24);

            dataToSend_Dio32 = Sys.SetBitOfInt(dataToSend_Dio32, 0, KP_ROT.relayDown);
            dataToSend_Dio32 = Sys.SetBitOfInt(dataToSend_Dio32, 1, KP_ROT.relayUp);
            dataToSend_Dio32 = Sys.SetBitOfInt(dataToSend_Dio32, 6, KP_ROT.ky3);
            //dataToSend_Sbc34 = Sys.SetBitOfInt(dataToSend_Sbc34, 0, KP_ROT.ky3);//20140220 ky3 sa istog dia ko i komande

            UdpTx.UniqueInstance.SendPDORaw(544, Sys._KompenzacioneCanNetworkId, new byte[8] { (byte)dataToSend_Dio32, 0, 0, 0, 0, 0, 0, 0 }, 0, 1);
            //UdpTx.UniqueInstance.SendPDORaw(546, Sys._KompenzacioneCanNetworkId, new byte[8] { (byte)dataToSend_Sbc34, 0, 0, 0, 0, 0, 0, 0 }, 0, 1);//20140220 ky3 sa istog dia ko i komande
            //Debug.WriteLine("===dio32={0:X}", dataToSend_Dio32);
            //Debug.WriteLine("===dio34={0:X}", dataToSend_Sbc34);
        }

    }


}
