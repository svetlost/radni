﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Threading;
//using StClient.Wagons;
using System.ComponentModel;

namespace StClient
{
    public class LockMotors : INotifyPropertyChanged
    {
        //#region UniqueInstance
        //class LockMotorsCreator
        //{
        //    static LockMotorsCreator() { }
        //    internal static readonly LockMotors uniqueInstance = new LockMotors();
        //}
        //public static LockMotors UniqueInstance
        //{
        //    get { return LockMotorsCreator.uniqueInstance; }
        //}
        //#endregion

        DispatcherTimer timer_CalculateLocks = new DispatcherTimer();
        DispatcherTimer timer_Timeout = new DispatcherTimer();
        DispatcherTimer timer_DisplayUpdate = new DispatcherTimer();

        DateTime _OverrideTimeStart, _OverrideTimeEnd, _OverrideTimeElapsed;
        TimeSpan _OverrideTimeRemaining, _OverrideSetTime;
        public DateTime OverrideTimeStart { get { return _OverrideTimeStart; } set { if (_OverrideTimeStart != value) { _OverrideTimeStart = value; OnNotify("OverrideTimeStart"); } } }
        public DateTime OverrideTimeEnd { get { return _OverrideTimeEnd; } set { if (_OverrideTimeEnd != value) { _OverrideTimeEnd = value; OnNotify("OverrideTimeEnd"); } } }
        public DateTime OverrideTimeElapsed { get { return _OverrideTimeElapsed; } set { if (_OverrideTimeElapsed != value) { _OverrideTimeElapsed = value; OnNotify("OverrideTimeElapsed"); } } }
        public TimeSpan OverrideTimeRemaining { get { return _OverrideTimeRemaining; } set { if (_OverrideTimeRemaining != value) { _OverrideTimeRemaining = value; OnNotify("OverrideTimeRemaining"); } } }
        public TimeSpan OverrideSetTime { get { return _OverrideSetTime; } set { if (_OverrideSetTime != value) { _OverrideSetTime = value; OnNotify("OverrideSetTime"); } } }

        bool _OverrideActive = false;
        public bool OverrideActive { get { return _OverrideActive; } set { if (_OverrideActive != value) { _OverrideActive = value; OnNotify("OverrideActive"); } } }
        bool _enableMotion = false;
        public bool enableMotion { get { return _enableMotion; } set { if (_enableMotion != value) { _enableMotion = value; OnNotify("enableMotion"); } } }

        #region platform definitions
        //MotorLogic OP1v, OP2v, OP3v, OP4v, OP5v;
        //MotorLogic SP1v, SP2v, SP3v, SP4v, KP1v, BAR;
        //MotorLogic M11v, M12v, M13v, M14v, M15v;
        //MotorLogic M21v, M22v, M23v, M24v, M25v;

        //MotorLogic OP1z, OP2z, OP3z, OP4z, OP5z;
        //MotorLogic SP3z, SP4z, KP1z;
        //MotorLogic M11z, M12z, M13z, M14z, M15z;
        //MotorLogic M21z, M22z, M23z, M24z, M25z;

        //KompenzacionaLogic003 KP_L1, KP_L2, KP_L3, KP_R1, KP_R2, KP_R3, KP_ROT;

        //VagonLogic003 WAG_L1, WAG_L2, WAG_L3, WAG_R1, WAG_R2, WAG_R3, WAG_ROT;

        //List<MotorLogic> OP_Moduli_v = new List<MotorLogic>();
        //List<MotorLogic> OP_Moduli_z = new List<MotorLogic>();
        //List<MotorLogic> SP1_Moduli_v = new List<MotorLogic>();
        //List<MotorLogic> SP2_Moduli_v = new List<MotorLogic>();
        //List<MotorLogic> SP1_Moduli_z = new List<MotorLogic>();
        //List<MotorLogic> SP2_Moduli_z = new List<MotorLogic>();
        #endregion

        //bool SP1_AllModulesInZero { get { return SP1_Moduli_v.All(q => Equal(q.CP, 0)); } }
        //bool SP2_AllModulesInZero { get { return SP2_Moduli_v.All(q => Equal(q.CP, 0)); } }

        //bool OP_AllModulesNotTilted { get { return OP_Moduli_z.All(q => Equal(q.CP, 0)); } }
        //bool OP_AllModulesInZero { get { return OP_Moduli_v.All(q => Equal(q.CP, 0)); } }

        //bool SP1_AllModulesNotTilted { get { return SP1_Moduli_z.All(q => Equal(q.CP, 0)); } }
        //bool SP1_AnyModuleTilted { get { return SP1_Moduli_z.Any(q => NotEqual(q.CP, 0)); } }
        //bool SP2_AllModulesNotTilted { get { return SP2_Moduli_z.All(q => Equal(q.CP, 0)); } }

        const double minHeightDelta = 3.0;

        public LockMotors()
        {
            #region attach motor references
            //OP1v = Sys.StaticMotorList.Find(q => q.MotorID == 1);
            //OP2v = Sys.StaticMotorList.Find(q => q.MotorID == 2);
            //OP3v = Sys.StaticMotorList.Find(q => q.MotorID == 3);
            //OP4v = Sys.StaticMotorList.Find(q => q.MotorID == 4);
            //OP5v = Sys.StaticMotorList.Find(q => q.MotorID == 5);

            //SP1v = Sys.StaticMotorList.Find(q => q.MotorID == 31);
            //SP2v = Sys.StaticMotorList.Find(q => q.MotorID == 32);
            //SP3v = Sys.StaticMotorList.Find(q => q.MotorID == 33);
            //SP4v = Sys.StaticMotorList.Find(q => q.MotorID == 34);

            //M11v = Sys.StaticMotorList.Find(q => q.MotorID == 11);
            //M12v = Sys.StaticMotorList.Find(q => q.MotorID == 12);
            //M13v = Sys.StaticMotorList.Find(q => q.MotorID == 13);
            //M14v = Sys.StaticMotorList.Find(q => q.MotorID == 14);
            //M15v = Sys.StaticMotorList.Find(q => q.MotorID == 15);

            //M21v = Sys.StaticMotorList.Find(q => q.MotorID == 21);
            //M22v = Sys.StaticMotorList.Find(q => q.MotorID == 22);
            //M23v = Sys.StaticMotorList.Find(q => q.MotorID == 23);
            //M24v = Sys.StaticMotorList.Find(q => q.MotorID == 24);
            //M25v = Sys.StaticMotorList.Find(q => q.MotorID == 25);

            //OP1z = Sys.StaticMotorList.Find(q => q.MotorID == 41);
            //OP2z = Sys.StaticMotorList.Find(q => q.MotorID == 42);
            //OP3z = Sys.StaticMotorList.Find(q => q.MotorID == 43);
            //OP4z = Sys.StaticMotorList.Find(q => q.MotorID == 44);
            //OP5z = Sys.StaticMotorList.Find(q => q.MotorID == 45);

            //KP1v = Sys.StaticMotorList.Find(q => q.MotorID == 81);
            //KP1z = Sys.StaticMotorList.Find(q => q.MotorID == 82);
            //BAR = Sys.StaticMotorList.Find(q => q.MotorID == 6);

            //M11z = Sys.StaticMotorList.Find(q => q.MotorID == 51);
            //M12z = Sys.StaticMotorList.Find(q => q.MotorID == 52);
            //M13z = Sys.StaticMotorList.Find(q => q.MotorID == 53);
            //M14z = Sys.StaticMotorList.Find(q => q.MotorID == 54);
            //M15z = Sys.StaticMotorList.Find(q => q.MotorID == 55);

            //M21z = Sys.StaticMotorList.Find(q => q.MotorID == 61);
            //M22z = Sys.StaticMotorList.Find(q => q.MotorID == 62);
            //M23z = Sys.StaticMotorList.Find(q => q.MotorID == 63);
            //M24z = Sys.StaticMotorList.Find(q => q.MotorID == 64);
            //M25z = Sys.StaticMotorList.Find(q => q.MotorID == 65);

            //SP3z = Sys.StaticMotorList.Find(q => q.MotorID == 71);
            //SP4z = Sys.StaticMotorList.Find(q => q.MotorID == 72);

            //KP_L1 = KompenzacioneRxTx.UniqueInstance.KP_L1;
            //KP_L2 = KompenzacioneRxTx.UniqueInstance.KP_L2;
            //KP_L3 = KompenzacioneRxTx.UniqueInstance.KP_L3;
            //KP_R1 = KompenzacioneRxTx.UniqueInstance.KP_R1;
            //KP_R2 = KompenzacioneRxTx.UniqueInstance.KP_R2;
            //KP_R3 = KompenzacioneRxTx.UniqueInstance.KP_R3;
            //KP_ROT = KompenzacioneRxTx.UniqueInstance.KP_ROT;

            //WAG_L1 = Sys.vagoniRxTx.wL1;
            //WAG_L2 = Sys.vagoniRxTx.wL2;
            //WAG_L3 = Sys.vagoniRxTx.wL3;
            //WAG_R1 = Sys.vagoniRxTx.wR1;
            //WAG_R2 = Sys.vagoniRxTx.wR2;
            //WAG_R3 = Sys.vagoniRxTx.wR3;
            //WAG_ROT = Sys.vagoniRxTx.wROT;

            //SP1_Moduli_v.Add(M11v);
            //SP1_Moduli_v.Add(M12v);
            //SP1_Moduli_v.Add(M13v);
            //SP1_Moduli_v.Add(M14v);
            //SP1_Moduli_v.Add(M15v);

            //SP2_Moduli_v.Add(M21v);
            //SP2_Moduli_v.Add(M22v);
            //SP2_Moduli_v.Add(M23v);
            //SP2_Moduli_v.Add(M24v);
            //SP2_Moduli_v.Add(M25v);

            //SP1_Moduli_z.Add(M11z);
            //SP1_Moduli_z.Add(M12z);
            //SP1_Moduli_z.Add(M13z);
            //SP1_Moduli_z.Add(M14z);
            //SP1_Moduli_z.Add(M15z);

            //SP2_Moduli_z.Add(M21z);
            //SP2_Moduli_z.Add(M22z);
            //SP2_Moduli_z.Add(M23z);
            //SP2_Moduli_z.Add(M24z);
            //SP2_Moduli_z.Add(M25z);

            //OP_Moduli_v.Add(OP1v);
            //OP_Moduli_v.Add(OP2v);
            //OP_Moduli_v.Add(OP3v);
            //OP_Moduli_v.Add(OP4v);
            //OP_Moduli_v.Add(OP5v);

            //OP_Moduli_z.Add(OP1z);
            //OP_Moduli_z.Add(OP2z);
            //OP_Moduli_z.Add(OP3z);
            //OP_Moduli_z.Add(OP4z);
            //OP_Moduli_z.Add(OP5z);
            #endregion

            timer_CalculateLocks.Interval = TimeSpan.FromMilliseconds(500);
            timer_CalculateLocks.Tick += timer_CalculateLocks_Tick;
            timer_CalculateLocks.Start();

            timer_Timeout.Tick += timer_Timeout_Tick; //stop override after set time elapsed

            timer_DisplayUpdate.Interval = TimeSpan.FromMilliseconds(1000);
            timer_DisplayUpdate.Tick += timer_DisplayUpdate_Tick;

            OverrideSetTime = TimeSpan.FromMinutes(5);
        }

        private void timer_Timeout_Tick(object sender, EventArgs e)
        {
            OverrideActive = false;
            timer_Timeout.Stop();
            timer_DisplayUpdate.Stop();
            OverrideTimeStart = OverrideTimeEnd = DateTime.Now;
            OverrideTimeRemaining = TimeSpan.Zero;
        }

        private void timer_DisplayUpdate_Tick(object sender, EventArgs e)
        {
            OverrideTimeRemaining = OverrideTimeEnd - DateTime.Now;
        }

        public void StartOverride()
        {
            OverrideTimeStart = DateTime.Now;
            OverrideTimeEnd = OverrideTimeStart + OverrideSetTime;
            OverrideTimeRemaining = DateTime.Now - OverrideTimeEnd;

            OverrideActive = true;

            timer_Timeout.Interval = OverrideSetTime;
            timer_Timeout.Start();
            timer_DisplayUpdate.Start();
        }

        public void StopOverride()
        {
            OverrideTimeRemaining = TimeSpan.Zero;

            OverrideActive = false;

            timer_Timeout.Stop();
            timer_DisplayUpdate.Stop();
        }

        public void SetTime(TimeSpan time) { this.OverrideSetTime = time; }

        void timer_CalculateLocks_Tick(object sender, EventArgs e)
        {
            foreach (var m in Sys.StaticMotorList) (m as ILock).Lock = false;

            #region old
            //(BAR as ILock).Lock = !(OP_AllModulesNotTilted);

            //(OP1v as ILock).Lock = !(Equal(OP1z.CP, 0) && Equal(KP1z.CP, 0));
            //(OP2v as ILock).Lock = !(Equal(OP2z.CP, 0) && Equal(KP1z.CP, 0));
            //(OP3v as ILock).Lock = !(Equal(OP3z.CP, 0) && Equal(KP1z.CP, 0));
            //(OP4v as ILock).Lock = !(Equal(OP4z.CP, 0) && Equal(KP1z.CP, 0));
            //(OP5v as ILock).Lock = !(Equal(OP5z.CP, 0) && Equal(KP1z.CP, 0));

            //(KP1v as ILock).Lock = !(!SP1_AnyModuleTilted);
            ////(KP1z as ILock).Lock = !(InCollisionZone(OP_Moduli_v.ToList<ICpCv>(), KP1v.CP, 20));
            ////(KP1z as ILock).Lock = !(!InCollisionZone(OP_Moduli_v, KP1v.CP, 20));

            //(M11v as ILock).Lock = !(WAG_ROT.LimNeg && Equal(M21z.CP, 0));
            //(M12v as ILock).Lock = !(WAG_ROT.LimNeg && Equal(M22z.CP, 0));
            //(M13v as ILock).Lock = !(WAG_ROT.LimNeg && Equal(M23z.CP, 0));
            //(M14v as ILock).Lock = !(WAG_ROT.LimNeg && Equal(M24z.CP, 0));
            //(M15v as ILock).Lock = !(WAG_ROT.LimNeg && Equal(M25z.CP, 0));

            //(M21v as ILock).Lock = (M22v as ILock).Lock = (M23v as ILock).Lock = (M24v as ILock).Lock = (M25v as ILock).Lock = !(WAG_ROT.LimNeg && WAG_L1.LimNeg && WAG_R1.LimNeg && Equal(SP3z.CP, 0));

            //(SP1v as ILock).Lock = !(WAG_ROT.LimNeg && SP1_AllModulesNotTilted && SP2_AllModulesNotTilted);
            //(SP2v as ILock).Lock = !(WAG_ROT.LimNeg && WAG_L1.LimNeg && WAG_R1.LimNeg && SP2_AllModulesNotTilted && Equal(SP3z.CP, 0));//ne sme da se dize ako je sp3 zakosen!!!
            //(SP3v as ILock).Lock = !(WAG_ROT.LimNeg && WAG_L2.LimNeg && WAG_R2.LimNeg && Equal(SP3z.CP, 0));
            //(SP4v as ILock).Lock = !(WAG_ROT.LimNeg && WAG_L3.LimNeg && WAG_R3.LimNeg && Equal(SP4z.CP, 0));

            //(WAG_L1 as ILock).Lock = !(WAG_ROT.LimNeg && WAG_R1.LimNeg && Equal(SP2v.CP, -87) && SP2_AllModulesInZero && SP2_AllModulesNotTilted && KP_L1.logic.LimNeg);
            //(WAG_L2 as ILock).Lock = !(WAG_ROT.LimNeg && WAG_R2.LimNeg && Equal(SP3v.CP, -28) && Equal(SP3z.CP, 0) && KP_L2.logic.LimNeg);
            //(WAG_L3 as ILock).Lock = !(WAG_ROT.LimNeg && WAG_R3.LimNeg && Equal(SP4v.CP, -28) && Equal(SP4z.CP, 0) && KP_L3.logic.LimNeg);

            //(WAG_R1 as ILock).Lock = !(WAG_ROT.LimNeg && WAG_L1.LimNeg && Equal(SP2v.CP, -87) && SP2_AllModulesInZero && SP2_AllModulesNotTilted && KP_R1.logic.LimNeg);
            //(WAG_R2 as ILock).Lock = !(WAG_ROT.LimNeg && WAG_L2.LimNeg && Equal(SP3v.CP, -28) && Equal(SP3z.CP, 0) && KP_R2.logic.LimNeg);
            //(WAG_R3 as ILock).Lock = !(WAG_ROT.LimNeg && WAG_L3.LimNeg && Equal(SP4v.CP, -28) && Equal(SP4z.CP, 0) && KP_R3.logic.LimNeg);

            //(WAG_ROT as ILock).Lock = !(WAG_L1.LimNeg && WAG_L2.LimNeg && WAG_L3.LimNeg && WAG_R1.LimNeg && WAG_R2.LimNeg && KP_ROT.logic.LimNeg && WAG_R3.LimNeg && Equal(SP1v.CP, -92.2) && Equal(SP2v.CP, -92.4) && Equal(SP3v.CP, -33.6) && Equal(SP4v.CP, -33.6) && SP1_AllModulesInZero && SP2_AllModulesInZero && SP1_AllModulesNotTilted && SP2_AllModulesNotTilted && Equal(SP3z.CP, 0) && Equal(SP4z.CP, 0));

            //(KP_L1 as ILock).Lock = !(WAG_L1.LimPos);
            //(KP_L2 as ILock).Lock = !(WAG_L2.LimPos);
            //(KP_L3 as ILock).Lock = !(WAG_L3.LimPos);
            //(KP_R1 as ILock).Lock = !(WAG_R1.LimPos);
            //(KP_R2 as ILock).Lock = !(WAG_R2.LimPos);
            //(KP_R3 as ILock).Lock = !(WAG_R3.LimPos);
            //(KP_ROT as ILock).Lock = !(WAG_ROT.LimPos);

            //(SP4z as ILock).Lock = !(Math.Abs(SP4v.CP - SP3v.CP) > minHeightDelta);
            //(SP3z as ILock).Lock = !(SP2_Moduli_v.All(q => Math.Abs(q.CP_Absolute - SP3v.CP) > minHeightDelta));//todo check this out 20140212

            //(M21z as ILock).Lock = !(Math.Abs(M21v.CP_Absolute - M11v.CP_Absolute) > minHeightDelta);
            //(M22z as ILock).Lock = !(Math.Abs(M22v.CP_Absolute - M12v.CP_Absolute) > minHeightDelta);
            //(M23z as ILock).Lock = !(Math.Abs(M23v.CP_Absolute - M13v.CP_Absolute) > minHeightDelta);
            //(M24z as ILock).Lock = !(Math.Abs(M24v.CP_Absolute - M14v.CP_Absolute) > minHeightDelta);
            //(M25z as ILock).Lock = !(Math.Abs(M25v.CP_Absolute - M15v.CP_Absolute) > minHeightDelta);

            //(M11z as ILock).Lock = (M12z as ILock).Lock = (M13z as ILock).Lock = (M14z as ILock).Lock = (M15z as ILock).Lock = !(SP1_Moduli_v.All(q => Math.Abs(q.CP_Absolute - KP1v.CP) > minHeightDelta));

            //(KP1z as ILock).Lock = !(OP_Moduli_v.All(q => Math.Abs(q.CP - KP1v.CP) > minHeightDelta));

            //(OP1z as ILock).Lock = !(Math.Abs(OP1v.CP - BAR.CP) > minHeightDelta);
            //(OP2z as ILock).Lock = !(Math.Abs(OP2v.CP - BAR.CP) > minHeightDelta);
            //(OP3z as ILock).Lock = !(Math.Abs(OP3v.CP - BAR.CP) > minHeightDelta);
            //(OP4z as ILock).Lock = !(Math.Abs(OP4v.CP - BAR.CP) > minHeightDelta);
            //(OP5z as ILock).Lock = !(Math.Abs(OP5v.CP - BAR.CP) > minHeightDelta);
            #endregion

        }

        bool Equal(double CP, double TargetP) { return Math.Abs(CP - TargetP) < 0.2; }
        bool NotEqual(double CP, double TargetP) { return Math.Abs(CP - TargetP) > 0.2; }
        //bool InCollisionZone(List<ICpCv> list, double subjectCP, double CPmargin)
        //{
        //    return !(subjectCP > list.Min(q => q.CP) - CPmargin && subjectCP < list.Max(q => q.CP) + CPmargin);
        //}
        bool InCollisionZone<T>(List<T> list, double subjectCP, double CPmargin) where T : ICpCv
        {
            return (subjectCP > list.Min(q => q.CP_Absolute) - CPmargin && subjectCP < list.Max(q => q.CP_Absolute) + CPmargin);
            //return (subjectCP > list.Min(q => q.CP) - CPmargin && subjectCP < list.Max(q => q.CP) + CPmargin);
        }
        //public bool t;

        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnNotify(String parameter)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(parameter));
            }
        }
        #endregion
    }
}
