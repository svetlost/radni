; Script generated by the Inno Setup Script Wizard.
; SEE THE DOCUMENTATION FOR DETAILS ON CREATING INNO SETUP SCRIPT FILES!

#define MyAppName "StClientKUW"
#define MyAppVersion "4.14.2.1"
#define MyAppPublisher "Svetlost teatar"
#define MyAppURL "www.steatar.rs"
#define MyAppExeName "StClient.exe"

[Setup]
; NOTE: The value of AppId uniquely identifies this application.
; Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)
AppId={{A1354184-FA93-4945-B4EE-F7913C3AB7E7}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
;AppVerName={#MyAppName} {#MyAppVersion}
AppPublisher={#MyAppPublisher}
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
AppUpdatesURL={#MyAppURL}
DefaultDirName={pf}\StClient
DisableDirPage=yes
DefaultGroupName={#MyAppName}
DisableProgramGroupPage=yes
OutputDir=c:\Users\T1PLT\Desktop\20160912 soft\SetupInnoFinal\
OutputBaseFilename=setupStClient
SetupIconFile=c:\Users\T1PLT\Desktop\20160912 soft\StClient\ikona client 000.ico
Compression=lzma
SolidCompression=yes
MinVersion=0,6.1
ArchitecturesInstallIn64BitMode=x64 ia64
UninstallDisplayIcon={app}\StClient.exe
PrivilegesRequired=admin
VersionInfoCompany=Svetlost teatar
VersionInfoProductName=StClient

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"
Name: "quicklaunchicon"; Description: "{cm:CreateQuickLaunchIcon}"; GroupDescription: "{cm:AdditionalIcons}"; MinVersion: 0,6.1

[Files]
Source: "c:\Users\T1PLT\Desktop\20160912 soft\StClient\bin\Debug\StClient.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "c:\Users\T1PLT\Desktop\20160912 soft\StClient\bin\Debug\LogAlertHB.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "c:\Users\T1PLT\Desktop\20160912 soft\StClient\bin\Debug\phidget21.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "c:\Users\T1PLT\Desktop\20160912 soft\StClient\bin\Debug\Phidget21.NET.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "c:\Users\T1PLT\Desktop\20160912 soft\StClient\bin\Debug\StClient.exe.config"; DestDir: "{app}"; Flags: ignoreversion
; NOTE: Don't use "Flags: ignoreversion" on any shared system files
Source: "c:\Users\T1PLT\Desktop\20160912 soft\StClient\bin\Debug\Properties\AIO.settings"; DestDir: "{app}\Properties"; Flags: ignoreversion
Source: "c:\Users\T1PLT\Desktop\20160912 soft\StClient\bin\Debug\Properties\CanSettings.settings"; DestDir: "{app}\Properties"; Flags: ignoreversion
Source: "c:\Users\T1PLT\Desktop\20160912 soft\StClient\bin\Debug\Properties\Settings.settings"; DestDir: "{app}\Properties"; Flags: ignoreversion
Source: "c:\Users\T1PLT\Desktop\20160912 soft\StClient\bin\Debug\Config\002Codelist_12_00_07.xml"; DestDir: "{app}\Config"; Flags: ignoreversion
Source: "c:\Users\T1PLT\Desktop\20160912 soft\StClient\bin\Debug\Config\204-034zamResources.xaml"; DestDir: "{app}\Config"; Flags: ignoreversion
Source: "c:\Users\T1PLT\Desktop\20160912 soft\StClient\bin\Debug\Config\Dio1000.xml"; DestDir: "{app}\Config"; Flags: ignoreversion
Source: "c:\Users\T1PLT\Desktop\20160912 soft\StClient\bin\Debug\Config\DrvPlc.xml"; DestDir: "{app}\Config"; Flags: ignoreversion
Source: "c:\Users\T1PLT\Desktop\20160912 soft\StClient\bin\Debug\Config\hoist_List.xml"; DestDir: "{app}\Config"; Flags: ignoreversion
Source: "c:\Users\T1PLT\Desktop\20160912 soft\StClient\bin\Debug\Config\L8400_List.xml"; DestDir: "{app}\Config"; Flags: ignoreversion
Source: "c:\Users\T1PLT\Desktop\20160912 soft\StClient\bin\Debug\Config\motors_List.xml"; DestDir: "{app}\Config"; Flags: ignoreversion
Source: "c:\Users\T1PLT\Desktop\20160912 soft\StClient\bin\Debug\Config\PhJoystick.xml"; DestDir: "{app}\Config"; Flags: ignoreversion
Source: "c:\Users\T1PLT\Desktop\20160912 soft\StClient\bin\Debug\Config\Rfids.xml"; DestDir: "{app}\Config"; Flags: ignoreversion
Source: "c:\Users\T1PLT\Desktop\20160912 soft\StClient\bin\Debug\Config\sbc_List.xml"; DestDir: "{app}\Config"; Flags: ignoreversion
Source: "c:\Users\T1PLT\Desktop\20160912 soft\StClient\bin\Debug\Config\tripListL9300-20.xml"; DestDir: "{app}\Config"; Flags: ignoreversion
Source: "c:\Users\T1PLT\Desktop\20160912 soft\CommonFiles\ikona client 000.ico"; DestDir: "{app}"; Flags: ignoreversion

[Icons]
Name: "{group}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"
Name: "{commondesktop}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; Tasks: desktopicon
Name: "{userappdata}\Microsoft\Internet Explorer\Quick Launch\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; Tasks: quicklaunchicon
Name: "{commonstartup}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; Tasks: desktopicon

[Run]
;Filename: "{app}\{#MyAppExeName}"; Description: "{cm:LaunchProgram,{#StringChange(MyAppName, '&', '&&')}}"; Flags: nowait postinstall skipifsilent

[Dirs]
Name: "{app}\Backup"; Flags: uninsneveruninstall; Permissions: everyone-full
Name: "{app}\Properties"; Flags: uninsalwaysuninstall
Name: "c:\Cues"; Flags: uninsneveruninstall
Name: "{app}\Config"

[Registry]
Root: "HKLM"; Subkey: "SYSTEM\CurrentControlSet\services\eventlog\Application\ST_CLIENT"; ValueType: string; ValueName: "EventMessageFile"; ValueData: "C:\Windows\Microsoft.NET\Framework\v2.0.50727\EventLogMessages.dll"; Flags: createvalueifdoesntexist

