﻿using StClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StConfig
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            //ReadMotorList = xMot.ReadXml("motors_List.xml", CreateDefaultRow.Create);

            ReadProjectList = xProj.ReadXml(Environment.CurrentDirectory + @"\Config\projects_List.xml", CreateDefaultRow.Create);
            ReadMotorList = xMot.ReadXml(Environment.CurrentDirectory + @"\Config\motors_Sumary.xml", CreateDefaultRow.Create);
            ReadClusterList = xCluster.ReadXml(Environment.CurrentDirectory + @"\Config\cluster_List.xml", CreateDefaultRow.Create);
            ReadRfidList = xRfid.ReadXml(Environment.CurrentDirectory + @"\Config\rfids_Sumary.xml", CreateDefaultRow.Create);

            if (ReadProjectList.Count == 0) { listboxLog.Items.Add("drives xml read ERROR!"); return; }
            dataGrid_Projects.ItemsSource = ReadProjectList;

            if (ReadMotorList.Count > 0)
            {
                //dataGrid_Drives.ItemsSource = ReadMotorList;
                listboxLog.Items.Add("drives xml read");
            }
            if (ReadClusterList.Count > 0)
            {
                //dataGrid_Clusters.ItemsSource = ReadClusterList;
                listboxLog.Items.Add("clusters xml read");
            }
            if (ReadRfidList.Count > 0)
            {
                //dataGrid_Rfids.ItemsSource = ReadRfidList;
                listboxLog.Items.Add("rfids xml read");
            }

        }

        List<MotorLogic> ReadMotorList, FilteredMotorList;
        List<StProject> ReadProjectList, selectedProject;
        List<StClusters> ReadClusterList, FilteredClusterList;
        List<StRfids> ReadRfidList, FilteredRfidList;
        XmlUtil<MotorLogic> xMot = new XmlUtil<MotorLogic>();
        XmlUtil<StProject> xProj = new XmlUtil<StProject>();
        XmlUtil<StClusters> xCluster = new XmlUtil<StClusters>();
        XmlUtil<StRfids> xRfid = new XmlUtil<StRfids>();
        XmlUtil<RfidItem> xRfidItem = new XmlUtil<RfidItem>();
        //StProject selectedProject { get; set; }

        private void buttonReadProjects_Click(object sender, RoutedEventArgs e)
        {
        }

        private void buttonReadDrives_Click(object sender, RoutedEventArgs e)
        {
        }

        private void buttonCreatConfig_Click(object sender, RoutedEventArgs e)
        {
            //if (dataGrid_Drives.ItemsSource == 0) return;
            //if (xMot.WriteXml(Environment.CurrentDirectory + @"\Config\motors_111.xml", FilteredMotorList)) listboxLog.Items.Add("motors_111 xml written"); ;
            if (xMot.WriteXml(Environment.CurrentDirectory + @"\Config\motors_List.xml", FilteredMotorList)) listboxLog.Items.Add("motors_List xml written");
            if (xCluster.WriteXml(Environment.CurrentDirectory + @"\Config\clusters.xml", FilteredClusterList)) listboxLog.Items.Add("clusters xml written");
            if (xProj.WriteXml(Environment.CurrentDirectory + @"\Config\selectedProj.xml", selectedProject)) listboxLog.Items.Add("selectedProj xml written");

            List<RfidItem> tempRfidList = new List<RfidItem>();
            foreach (StRfids rfi in FilteredRfidList) tempRfidList.Add(new RfidItem() { Name = rfi.userName, RfidTag = rfi.userID });
            if (xRfidItem.WriteXml(Environment.CurrentDirectory + @"\Config\Rfids.xml", tempRfidList)) listboxLog.Items.Add("Rfids xml written");
        }

        private void dataGrid_Projects_GotFocus(object sender, RoutedEventArgs e)
        {
            RoutedEventArgs e1 = e;


        }

        private void dataGrid_Projects_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            MouseButtonEventArgs e1 = e;
        }

        private void dataGrid_Projects_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (dataGrid_Projects.SelectedIndex == -1) return;

            int selectedProjectID = ((StProject)dataGrid_Projects.SelectedItem).projectID;

            FilteredMotorList = ReadMotorList.Where(m => m.ProjectID == selectedProjectID).ToList();
            FilteredClusterList = ReadClusterList.Where(m => m.projectID == selectedProjectID).ToList();
            selectedProject = ReadProjectList.Where(m => m.projectID == selectedProjectID).ToList();
            FilteredRfidList = ReadRfidList.Where(m => m.projectID == selectedProjectID).ToList();

            if (FilteredMotorList.Count() == 0) { dataGrid_Drives.ItemsSource = null; dataGrid_Drives.Items.Clear(); }
            else dataGrid_Drives.ItemsSource = FilteredMotorList;

            if (FilteredClusterList.Count() == 0) { dataGrid_Clusters.ItemsSource = null; dataGrid_Clusters.Items.Clear(); }
            else dataGrid_Clusters.ItemsSource = FilteredClusterList;

            if (FilteredRfidList.Count() == 0) { dataGrid_Rfids.ItemsSource = null; dataGrid_Rfids.Items.Clear(); }
            else dataGrid_Rfids.ItemsSource = FilteredRfidList;

        }

        private void dataGrid_Projects_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void buttonLoadSelected_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
