﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StClient
{
    class L8400GroupLogic : INotifyPropertyChanged
    {
        int _Sv;
        public int Sv { get { return _Sv; } set { if (_Sv != value) { _Sv = value; OnNotify("Sv"); } } }
        bool _Up, _Down;
        public bool Up { get { return _Up; } set { if (_Up != value) { _Up = value; OnNotify("Up"); } } }
        public bool Down { get { return _Down; } set { if (_Down != value) { _Down = value; OnNotify("Down"); } } }


        public L8400GroupLogic() { }

        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnNotify(String parameter)
        {
            if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(parameter));
        }
        #endregion
    }
}
