﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows.Threading;
using StClient.Wagons;

namespace StClient
{
    public class KompenzacionaLogic003 : INotifyPropertyChanged, ILock
    {
        public VagonLogic003 logic { get; set; }

        string _Title;
        public string Title { get { return _Title; } set { if (_Title != value) { _Title = value; OnNotify("Title"); } } }

        bool _up = false, _down = false, _relayUp = false, _relayDown = false, _ky3 = false, _STOP = false, _enableUpDown=false;
        public bool UP { get { return _up; } set { if (_up != value) { _up = value; OnNotify("UP"); KompenzacioneRxTx.UniqueInstance.ProcessSM(this); } } }
        public bool DOWN { get { return _down; } set { if (_down != value) { _down = value; OnNotify("DOWN"); KompenzacioneRxTx.UniqueInstance.ProcessSM(this); } } }
        public bool relayUp { get { return _relayUp; } set { if (_relayUp != value) { _relayUp = value; OnNotify("relayUp"); } } }
        public bool relayDown { get { return _relayDown; } set { if (_relayDown != value) { _relayDown = value; OnNotify("relayDown"); } } }
        public bool ky3 { get { return _ky3; } set { if (_ky3 != value) { _ky3 = value; OnNotify("ky3"); } } }
        public bool STOP { get { return _STOP; } set { if (_STOP != value) { _STOP = value; OnNotify("STOP"); KompenzacioneRxTx.UniqueInstance.ProcessSM(this); } } }
        public bool enableUpDown { get { return _enableUpDown; } set { if (_enableUpDown != value) { _enableUpDown = value; OnNotify("enableUpDown"); } } }

        bool _Lock;
        public bool Lock { get { return _Lock; } set { if (_Lock != value) { _Lock = value; OnNotify("Lock"); } } }//safety lock - if motion is not safe block motion


        public KompenzacionaLogic003(int CanAddr, string title)
        {
            _Title = title;
            logic = new VagonLogic003(CanAddr, title);
            State = oldState = SM_State.SM_Idle;

        }


        public SM_State State { get; set; }
        public SM_State oldState { get; set; }

        public void TotalStop() { STOP = true; }

        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnNotify(String parameter)
        {
            if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(parameter));
        }
        #endregion

    }

}
