﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StClient
{
    class DualMotorLogic
    {
        MotorLogic mot1, mot2;
        public DualMotorLogic(MotorLogic _motor1, MotorLogic _motor2)
        {
            mot1 = _motor1;//UVEK SE GLEDA POZICIJA MOT1!
            mot2 = _motor2;
        }
        public double LimNeg { get { return Math.Max(mot1.LimNeg, mot2.LimNeg); } }
        public double LimPos { get { return Math.Min(mot1.LimPos, mot2.LimPos); } }
        public double Vmax { get { return Math.Min(mot1.MaxV, mot2.MaxV); } }

        public double deltaH { get { return Math.Abs(mot1.CP - mot2.CP); } }
        public bool leveled { get { return deltaH < 0.01; } } //todo uniti koliko resiti
    }
}
