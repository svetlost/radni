﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Diagnostics;
using StClient.Properties;

namespace StClient
{
    /// <summary>
    /// Interaction logic for Test.xaml
    /// </summary>
    public partial class Test2 : UserControl
    {
        List<CheckBox> selectedMotors = new List<CheckBox>(); 
        DispatcherTimer timerX = new DispatcherTimer();
        TimeSpan TimeInit = TimeSpan.FromMilliseconds(300);
        int __t = 0;

        Properties.Settings Sett = Properties.Settings.Default;
        Properties.CanSettings CanSett = Properties.CanSettings.Default;

        public Test2()
        {
            InitializeComponent();
            for (int i = 11; i < 26; i++)
            {
                if (i > 15 && i < 21) continue;
                CheckBox t = new CheckBox() { IsChecked = false, Content = "MODUL " + i.ToString(), Tag = i };
                selectedMotors.Add(t);
                MotorSelection.Children.Add(t);
            }
            timerX.Interval = TimeInit;
            timerX.Tick += new EventHandler(timerX_Tick);
        }

        void timerX_Tick(object sender, EventArgs e)
        {
            int time = Convert.ToInt32(_time.Text);
            __t += 1;
            foreach (CheckBox cb in selectedMotors.FindAll(q => q.IsChecked == true))
            {
                int mID = (int)cb.Tag;
                double b = Convert.ToDouble(_b.Text);
                double c = Convert.ToDouble(_c.Text);
                double k = Convert.ToDouble(_k.Text);
                double X = (Math.Sin(b * __t + mID * c) * time * 0.001 * k);
                //Debug.WriteLine(X + DateTime.Now.ToLongTimeString());
                X = (X > 100 ? 100 : X);
                X = (X < -100 ? -100 : X);
                //double X = 200 * Math.Sin((dt * mID) + (t.TotalMilliseconds * w)) - 100;
                UdpTx.UniqueInstance.SendSDOWriteRQ(mID, CanSett.MvIndex, CanSett.MvSubIndex, Convert.ToInt32(Math.Abs(X)), SdoPriority.hi);
                //slider.Value = X;
                if (X > 0)
                    UdpTx.UniqueInstance.SendPDO(mID, CanSett.CanCommandManPositive, 0,8);
                else if (X < 0)
                    UdpTx.UniqueInstance.SendPDO(mID, CanSett.CanCommandManNegative, 0, 8);
                else
                    UdpTx.UniqueInstance.SendPDO(mID, CanSett.CanCommandStop, 0, 8);
            }
        }

        private void InitButton_Click(object sender, RoutedEventArgs e)
        {
            foreach (CheckBox c in selectedMotors.FindAll(w => w.IsChecked == true))
            {
                UdpTx.UniqueInstance.SendSDOWriteRQ((int)c.Tag, CanSett.SpIndex, CanSett.SpSubIndex, Convert.ToInt32(_INITPOS.Text), SdoPriority.hi);
                //UdpTx.UniqueInstance.SendSDOWriteRQ((int)c.Tag, CanStructure.SpIndex, CanStructure.SpSubIndex, Convert.ToInt32(_INITPOS.Text), SdoPriority.hi);
                UdpTx.UniqueInstance.SendSDOWriteRQ((int)c.Tag, CanSett.SvIndex, CanSett.SvSubIndex, Convert.ToInt32(_SV.Text), SdoPriority.hi);
                UdpTx.UniqueInstance.SendPDO((int)c.Tag, CanSett.CanCommandAuto, 300, 2);
                UdpTx.UniqueInstance.SendPDO((int)c.Tag, CanSett.CanCommandIdle, 2200, 2);
            }
            timerX.Interval = TimeSpan.FromMilliseconds(1000);
        }

        private void StopButton_Click(object sender, RoutedEventArgs e)
        {
            timerX.Stop();
            foreach (CheckBox c in selectedMotors.FindAll(w => w.IsChecked == true))
            {
                UdpTx.UniqueInstance.SendPDO((int)c.Tag, CanSett.CanCommandStop, 300, 2);
                UdpTx.UniqueInstance.SendPDO((int)c.Tag, CanSett.CanCommandIdle, 2200, 2);
            }
        }

        private void StartButton_Click(object sender, RoutedEventArgs e)
        {
            __t = 0;
            timerX.Interval = TimeSpan.FromMilliseconds(Convert.ToDouble(_time.Text));
            timerX.Start();
        }

    }
}
