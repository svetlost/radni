﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.ComponentModel;
using System.Windows;
using System.Collections.ObjectModel;
using System.Windows.Threading;
using System.Windows.Input;
using System.Diagnostics;
using System.Windows.Data;
using System.Windows.Media;
using LogAlertHB;

namespace StClient
{



    public class MotorLogic : IMotorIndicators, IMv, ISpSv, ISyncIMotorsList, ILock, ICpCv, INotifyPropertyChanged, ILinkCheck
    {
        [XmlIgnore]
        Properties.Settings Sett = Properties.Settings.Default;
        [XmlIgnore]
        Properties.CanSettings CanSett = Properties.CanSettings.Default;
        //[XmlIgnore]
        //public bool BtnChanged { get; set; } //todo notused. cleanup
        [XmlIgnore]
        CounterSumsLogic imotTotalLogic = new CounterSumsLogic();
        [XmlIgnore]
        public bool AutoBrakingTimeStampTaken = false;
        [XmlIgnore]
        public bool StopSend = false;
        //[XmlIgnore]
        //public DateTime autoStartedTimeStamp;
        [XmlIgnore]
        public double previousCP;
        [XmlIgnore]
        public DateTime AutoStarting2_waitTime;
        [XmlIgnore]
        public bool previousBrake = true;
        public delegate void CallCues(MotorLogic motor, CueUpdateAction Add);
        public event CallCues NotifyCueEventHandler;
        [XmlIgnore]
        public int CanResponseCount = 0;
        [XmlIgnore]
        public int StoppingCounter = 0;
        [XmlIgnore]
        public double PreviousJoystickMV = 0;


        [XmlIgnore]
        public IMotorIndicators iMotorIndicators;
        [XmlIgnore]
        public IMv iMv;
        [XmlIgnore]
        public ISpSv iSpSv;
        [XmlIgnore]
        public ISyncIMotorsList calculatesEnabledType { get; set; }


        Visibility _visibility = Visibility.Collapsed;
        [XmlIgnore]
        public Visibility CanOffVisibility { get { return _visibility; } set { if (_visibility != value) { _visibility = value; OnNotify("CanOffVisibility"); } } }


        public List<MotorLogic> ListOfMotors()
        {
            return null;
        }

        [XmlIgnore]
        public int PreviousDirection = Properties.CanSettings.Default.CanCommandStop;
        [XmlIgnore] 
        public double PreviousMV = 0;

        int direction;
        [XmlIgnore]
        public int Direction { get { return CanSett.CanCommandStop; } set { direction = value; } }


        [XmlIgnore]
        public bool IsJoystickState = false;


        bool _isync = false;
        [XmlIgnore]
        public bool IsSync { get { return _isync; } set { _isync = value; } }

        double _syncvalue = 1;
        [XmlIgnore]
        public double SyncValue { get { return _syncvalue; } set { _syncvalue = value; } }



        //public enum CueUpdateAction { Add, Remove };

        //[XmlIgnore]
        //public Movement MovementControler = Movement.NoMovement;

        [XmlIgnore]
        public ObservableCollection<TripHistoryItem> TripHistoryList = new ObservableCollection<TripHistoryItem>();
        [XmlIgnore]
        public ObservableCollection<TripHistoryItem> TripHistoryGet
        { get { return TripHistoryList; } }

        [XmlIgnore]
        public List<int> ListOfCueNumbers = new List<int>();

        [XmlIgnore]
        public DateTime ManTimeOut { get; set; }

        //string _type = "";
        double _sp = 300, _CueSP;
        double _sv = 100;
        double _cp;
        //double _prevCP;
        double _tp;
        double _cv;
        double _mv = 100;
        int _rampTime = 1500;
        double _limNeg = -300;
        double _limPos = 1000;
        bool _enabled = true;
        bool _inverted = false;
        double _coeficient = 60467.8848;
        double _MaxV = 10;
        double _DefaultMV = 20;
        int _ProjectID = 7;
        int _motorID = 1;
        int _group = -1;
        bool _cued = false;
        short _canAddress = 11;
        byte _canChannel = 1;
        int _sdoChannel = 1, _SdoCanMsgID;
        bool _canRecived;
        string _title = "QWERTYU";
        string _description = "";
        byte[] _sdo;
        byte[] _pdo;
        bool _trip, _refOk, _inh28, _moving, _brake, _local, _can, _limPosReached, _limNegReached, _MainContactor;
        Visibility _GuiVisibility = Visibility.Visible;
        //bool cueRunning = false;
        double _Imot;
        byte _ClusterLogical, _ClusterElectric;
        //string _DiagItems = @"NACT/51/0/Rpm;MSET/56/0/%;C-UTIL/64/0/%;C-TEMP/61/0/oC;MOT-V/52/0/V;DC-V/53/0/V;MOT-I/54/0/A";
        string _DiagItems = @"";
        //double _abscv;

        //bool _JButton;
        //[XmlIgnore]
        //public bool JButton { get { return _JButton; } set { if (_JButton != value) { _JButton = value; OnNotify("JButton"); } } }
        //double _jv;
        //[XmlIgnore]
        //public double JV { get { return _jv; } set { if (_jv != value) { _jv = value; OnNotify("JV"); } } }
        //double _TX_JV, _TX_JV_OLD;
        //[XmlIgnore]
        //public double TX_JV { get { return _TX_JV; } set { if (_TX_JV != value) { _TX_JV = value; OnNotify("TX_JV"); } } }
        //[XmlIgnore]
        //public double TX_JV_OLD { get { return _TX_JV_OLD; } set { if (_TX_JV_OLD != value) { _TX_JV_OLD = value; OnNotify("TX_JV_O"); } } }
        [XmlIgnore]
        public string Description { get { return _description; } set { if (_description != value) { _description = value; OnNotify("a"); } } }
        [XmlIgnore]
        public bool CanRecived { get { return _canRecived; } set { _canRecived = value; } }
        [XmlAttribute("ProjectID")] //!!!@@@!!!
        public int ProjectID { get { return _ProjectID; } set { _ProjectID = value; } }
        [XmlAttribute("MotorID")]
        public int MotorID { get { return _motorID; } set { _motorID = value; } }
        [XmlAttribute("CanAddress")]
        public short CanAddress { get { return _canAddress; } set { _canAddress = value; } }
        [XmlAttribute("Title")]
        public string Title { get { return _title; } set { if (_title != value) { _title = value; OnNotify("Title"); } } }
        string _CabinetTitle;
        [XmlAttribute("CabinetTitle")]
        public string CabinetTitle { get { return _CabinetTitle; } set { if (_CabinetTitle != value) { _CabinetTitle = value; OnNotify("CabinetTitle"); } } }
        //[XmlAttribute("Type")]
        //public string Types { get { return _type; } set { _type = value; } }

        [XmlIgnore]
        public bool Cued
        {
            get { return _cued; }
            set
            {
                if (_cued != value)
                {
                    _cued = value;

                    if (value == true)
                        NotifyCueEventHandler(this, CueUpdateAction.Add);
                    //NotifyCueEventHandler(this, true);
                    else
                        NotifyCueEventHandler(this, CueUpdateAction.Remove);
                    //NotifyCueEventHandler(this, false);

                    OnNotify("Cued");
                }
            }
        }

        [XmlIgnore]
        public int GroupNo
        {
            ///obavezno je dva puta 
            ///_group = value;
            /// OnNotify("GroupNo");
            /// zbog glupog update buga sa listboxom


            get { return _group; }
            set
            {
                if (_group != value)
                {
                    if (_group != -1) //trenutna vrednost
                    {
                        Sys.StaticGroupList[_group].RemoveFromGroup(this);
                    }
                    if (value != -1)//nova vrednost
                    {
                        Sys.StaticGroupList[value].AddToGroup(this);
                        iMotorIndicators = Sys.StaticGroupList[value];
                        //iMv = Sys.StaticGroupList[value];//20140530 svaki motor racuna svoj joyVelocity
                        iMv = this;
                        calculatesEnabledType = Sys.StaticGroupList[value]; ;
                    }

                    if (value == -1) //nova vrednost
                    {
                        state = states.Idle;
                        iMotorIndicators = this;
                        iMv = this;
                        calculatesEnabledType = this;

                    }

                    Log.WriteDetailed(string.Format("MotorLogic id={0} title={1}: group changed. Old value={2}, new value={3}", this.MotorID, this.Title, _group, value));

                    _group = value;
                    OnNotify("GroupNo");
                    //CalculateEnableds(); // potrebno da bi se disablovali dugmici kad je u grupi i enablovali kad izadje //sklonjeno 20140406 - vec se izvrsava u tajmeru

                }
            }
        }
        bool _groupnochange;
        [XmlIgnore]
        public bool GroupChangeEnabled { get { return _groupnochange; } set { if (_groupnochange != value) { _groupnochange = value; OnNotify("GroupChangeEnabled"); } } }
        //[XmlIgnore]
        //public double absCV { get { return _abscv; } set { if (_abscv != value) { _abscv = value; OnNotify("absCV"); } } }
        [XmlIgnore]
        public double SP { get { return _sp; } set { if (_sp != value) { _sp = value; OnNotify("SP"); } } }
        [XmlIgnore]
        public double CueSP { get { return _CueSP; } set { if (_CueSP != value) { _CueSP = value; OnNotify("CueSP"); } } }
        //[XmlIgnore]
        //public double CueStartPos { get; set; }
        //[XmlIgnore]
        //public bool CueRunning { get { return cueRunning; } set { cueRunning = value; } }
        [XmlIgnore]
        public double SV { get { return _sv; } set { if (_sv != value) { _sv = value; OnNotify("SV"); } } }
        [XmlIgnore]
        public double CP
        {
            get { return _cp; }
            set
            {
                if (_cp != value)
                {
                    if (Sys.render3D) AnimateMotor.StartAnimation(MotorID, _cp, value);//u getsetu se okida animacija ako se promeni CP
                    //animateMotor.startAnimation(MotorID,  value);//u getsetu se okida animacija ako se promeni CP
                    //Debug.WriteLine("{0:hh mm ss.fff} cp={1}", DateTime.Now, _cp);
                    _cp = value;
                    OnNotify("CP");

                    //motor is a parent
                    if (ChildMotors.Count > 0)
                        foreach (var m in ChildMotors) m.UpdateCpAbsolute();
                    //motor is a child
                    else if (ParentMotor != null)
                        OnNotify("CP_Absolute");

                    //if(MotorID==40) Sys.printStack(this);
                }
            }
        }
        public void UpdateCpAbsolute() { OnNotify("CP_Absolute"); }
        double _CP_Absolute;
        [XmlIgnore]
        public double CP_Absolute
        {
            get
            {
                //if (MotorID == 14 || MotorID == 24) Debug.WriteLine("ID={3} cp={0} parentCP={1} cpAbs={2}", CP, ParentMotor.CP, CP + ParentMotor.CP, MotorID);
                if (ParentMotor == null) return _cp;
                else return _cp + ParentMotor.CP + 58.6; //20140406 proveriti ovo i staviti 58.6 u settings il negde (jednom za sc1 jednom za sc2)

            }
        }
        //[XmlIgnore]
        //public double prevCP { get { return _prevCP; } set { if (_prevCP != value) { _prevCP = value; OnNotify("prevCP"); } } }
        bool _Lock;
        [XmlIgnore]
        public bool Lock { get { return _Lock; } set { if (_Lock != value) { _Lock = value; OnNotify("Lock"); } } }//safety lock - if motion is not safe block motion
        [XmlIgnore]
        public double TP { get { return _tp; } set { if (_tp != value) { _tp = value; OnNotify("TP"); } } }//target pos
        [XmlIgnore]
        public double CV { get { return _cv; } set { if (_cv != value) { _cv = value; OnNotify("CV"); } } } //absCV = Math.Abs(value); - cemu l
        [XmlIgnore]
        public double MV { get { return _mv; } set { if (_mv != value) { _mv = value; OnNotify("MV"); } } }//20131227 todo kandidat za brisanje, ne koristi se
        [XmlAttribute("ParentMotorID")]
        public int ParentMotorID { get; set; }
        [XmlIgnore]
        public MotorLogic ParentMotor { get; set; }
        [XmlIgnore]
        public List<MotorLogic> ChildMotors = new List<MotorLogic>();
        [XmlAttribute("MaxV")]
        public double MaxV { get { return _MaxV; } set { if (_MaxV != value) { _MaxV = value; OnNotify("MaxV"); } } }

        double _BigTicks = double.NaN;
        [XmlAttribute("BigTicks")]
        public double BigTicks { get { return _BigTicks; } set { _BigTicks = value; } }

        [XmlAttribute("DefaultMV")]
        public double DefaultMV { get { return _DefaultMV; } set { if (_DefaultMV != value) { _DefaultMV = value; OnNotify("DefaultMV"); } } }
        [XmlAttribute("RampTime")]
        public int RampTime { get { return _rampTime; } set { _rampTime = value; } }
        [XmlAttribute("LimNeg")]
        public double LimNeg { get { return _limNeg; } set { if (_limNeg != value) { _limNeg = value; OnNotify("LimNeg"); } } }
        [XmlAttribute("LimPos")]
        public double LimPos { get { return _limPos; } set { if (_limPos != value) { _limPos = value; OnNotify("LimPos"); } } }
        [XmlAttribute("SDO_Channel")]
        public int SdoChannel { get { return _sdoChannel; } set { _sdoChannel = value; } }
        [XmlIgnore]
        public int SdoCanMsgID { get { return _SdoCanMsgID; } set { _SdoCanMsgID = value; } }
        [XmlAttribute("Enabled")]
        public bool Enabled { get { return _enabled; } set { _enabled = value; } }
        [XmlAttribute("Inverted")]
        public bool Inverted { get { return _inverted; } set { _inverted = value; } }
        [XmlAttribute("CAN_BUS")]
        public byte CanChannel { get { return _canChannel; } set { _canChannel = value; } }
        [XmlAttribute("Coeficient")]
        public double Coeficient { get { return _coeficient; } set { _coeficient = value; } }

        [XmlAttribute("ClusterLogical")]//logical group of drives such as stage platforms, sidestage hoists ...
        public byte ClusterLogical { get { return _ClusterLogical; } set { _ClusterLogical = value; } }
        [XmlAttribute("ClusterElectric")]//electrical circuit that supplies the drive. used for power consumption calc
        public byte ClusterElectric { get { return _ClusterElectric; } set { _ClusterElectric = value; } }

        double _GraphMin, _GraphMax;
        [XmlAttribute("GraphMin")]
        public double GraphMin { get { return _GraphMin; } set { _GraphMin = value; } }
        [XmlAttribute("GraphMax")]
        public double GraphMax { get { return _GraphMax; } set { _GraphMax = value; } }
        int _name3d;
        [XmlAttribute("Name3D")]
        public int Name3D { get { return _name3d; } set { _name3d = value; } }


        [XmlIgnore]
        public double Imot { get { return _Imot; } set { if (_Imot != value) { _Imot = value; OnNotify("Imot"); } } }
        double _LimPosFromServo, _LimNegFromServo;
        [XmlIgnore]
        public double LimPosFromServo { get { return _LimPosFromServo; } set { if (_LimPosFromServo != value) { _LimPosFromServo = value; OnNotify("LimPosFromServo"); } } }
        [XmlIgnore]
        public double LimNegFromServo { get { return _LimNegFromServo; } set { if (_LimNegFromServo != value) { _LimNegFromServo = value; OnNotify("LimNegFromServo"); } } }

        [XmlIgnore]
        public Visibility GuiVisibility { get { return _GuiVisibility; } set { if (_GuiVisibility != value) { _GuiVisibility = value; OnNotify("GuiVisibility"); } } }
        //20110224 bila je ideja da se resi sto preskace kad ima hajdovanih itema u listview (a treba i da se disabluju da bi sve bilo ok), ali ce to morati da se resi neki drugi put
        //[XmlIgnore]
        //public bool GuiEnabled { get { return _GuiEnabled; } set { if (_GuiEnabled != value) { _GuiEnabled = value; OnNotify("GuiEnabled"); } } }

        [XmlIgnore]
        public byte[] SDO_CanMsgID { get { return _sdo; } set { _sdo = value; } }
        [XmlIgnore]
        public byte[] PDO_CanMsgID { get { return _pdo; } set { _pdo = value; } }
        [XmlIgnore]
        public bool Trip
        {
            get { return _trip; }
            set
            {
                if (_trip != value)
                {
                    _trip = value;

                    if (value)
                    {
                        //UdpTx.UniqueInstance.SendSDOReadRQ(MotorID, 168, 0, SdoPriority.lo);
                        UdpTx.UniqueInstance.SendSDOReadRQ(MotorID, 168, 0, SdoPriority.lo);
                    }
                    LogAndNotify("Trip", value);
                }
            }
        }
        [XmlIgnore]
        public bool MainContactor { get { return _MainContactor; } set { if (_MainContactor != value) { _MainContactor = value; LogAndNotify("MainContactor", value); } } }
        [XmlIgnore]
        public bool RefOk { get { return _refOk; } set { if (_refOk != value) { _refOk = value; LogAndNotify("RefOk", value); } } }
        [XmlIgnore]
        public bool Inh28 { get { return _inh28; } set { if (_inh28 != value) { _inh28 = value; LogAndNotify("Inh28", value); } } }
        [XmlIgnore]
        public int MovingCounter { get; set; }
        [XmlIgnore]
        public bool Moving
        {
            get { return _moving; }
            set
            {
                if (_moving != value)
                {
                    _moving = value; LogAndNotify("Moving", value);
                    //Sys.printStack(this);
                }
            }
        }
        [XmlIgnore]
        public bool LimPosReached { get { return _limPosReached; } set { if (_limPosReached != value) { _limPosReached = value; LogAndNotify("LimPosReached", value); } } }
        [XmlIgnore]
        public bool LimNegReached { get { return _limNegReached; } set { if (_limNegReached != value) { _limNegReached = value; LogAndNotify("LimNegReached", value); } } }
        [XmlIgnore]
        public bool Brake { get { return _brake; } set { if (_brake != value) { _brake = value; LogAndNotify("Brake", value); } } }
        [XmlIgnore]
        public bool Local { get { return _local; } set { if (_local != value) { _local = value; LogAndNotify("Local", value); } } }


        public void SubscribeToCueLogic(CueLogic subject)
        {
            subject.NotifyMlEventHandler += UpdateFromCue;
        }

        void UpdateFromCue(int cueNumber, int MotorID, CueDelegateStates state)
        {
            if (this.MotorID == MotorID)
            {
                switch (state)
                {
                    case CueDelegateStates.Add:
                        ListOfCueNumbers.Add(cueNumber);
                        break;
                    case CueDelegateStates.Remove:
                        ListOfCueNumbers.Remove(cueNumber);
                        break;
                    case CueDelegateStates.Clear:
                        ListOfCueNumbers.Clear();
                        break;
                }
            }
        }

        #region   VALUES TO OBSERVE

        [XmlIgnore]
        public bool manPosNegEnabled { get { return _manPosNegEnabled; } set { if (_manPosNegEnabled != value) { _manPosNegEnabled = value; OnNotify("manPosNegEnabled"); } } }
        [XmlIgnore]
        public bool autoEnabled { get { return _autoEnabled; } set { if (_autoEnabled != value) { _autoEnabled = value; OnNotify("autoEnabled"); } } }
        [XmlIgnore]
        public bool stopEnabled { get { return _stopEnabled; } set { if (_stopEnabled != value) { _stopEnabled = value; OnNotify("stopEnabled"); } } }
        [XmlIgnore]
        public bool manEnabled { get { return _manEnabled; } set { if (_manEnabled != value) { _manEnabled = value; OnNotify("manEnabled"); } } }
        [XmlIgnore]
        public bool TempCan;

        bool _autoEnabled, _stopEnabled, _manEnabled, _manPosNegEnabled, _grpResetEnabled;//_autoEnabledCues


        [XmlIgnore]
        public bool Can { get { return _can; } set { if (_can != value) { _can = value; OnNotify("Can"); OnNotify("LinkIndicator"); } } }
        #endregion


        [XmlAttribute("DiagItems")]
        public string DiagItems { get { return _DiagItems; } set { if (_DiagItems != value) { _DiagItems = value; OnNotify("DiagItems"); } } }

        bool _spenabled, _mvenabled;
        [XmlIgnore]
        public bool SpSvEnabled { get { return _spenabled; } set { if (_spenabled != value) { _spenabled = value; OnNotify("SpSvEnabled"); } } }
        [XmlIgnore]
        public bool MVEnabled { get { return _mvenabled; } set { if (_mvenabled != value) { _mvenabled = value; OnNotify("MVEnabled"); } } }
        [XmlIgnore]
        public bool grpResetEnabled { get { return _grpResetEnabled; } set { if (_grpResetEnabled != value) { _grpResetEnabled = value; OnNotify("grpResetEnabled"); } } }

        [XmlIgnore]
        public int PosDirection;
        [XmlIgnore]
        public int NegDirection;
        //[XmlIgnore]
        //public int joystickDirection;
        [XmlIgnore]
        public double gdcDenum1203 { get; set; }
        [XmlIgnore]
        public double gdcNum1202 { get; set; }
        [XmlIgnore]
        public double gdcFeed1204 { get; set; }
        [XmlIgnore]
        public double gdcLimPos1223 { get; set; }
        [XmlIgnore]
        public double gdcLimNeg1224 { get; set; }
        [XmlIgnore]
        public double gdcCoef { get; set; }
        [XmlIgnore]
        public double ResRefCounter { get; set; }



        int _SecBrkControlAddress = 30, _SecBrkControlBit = 2;
        MotorBrake _SecBrkControlLock = MotorBrake.Locked;
        [XmlAttribute("SecBrkControlAddress")]
        public int SbcAddress { get { return _SecBrkControlAddress; } set { if (_SecBrkControlAddress != value) { _SecBrkControlAddress = value; OnNotify("SecBrkControlAddress"); } } }
        [XmlAttribute("SecBrkControlBit")]
        public int SbcBit { get { return _SecBrkControlBit; } set { if (_SecBrkControlBit != value) { _SecBrkControlBit = value; OnNotify("SecBrkControlBit"); } } }
        [XmlIgnore]
        public SecondaryBreakControl sbc;
        [XmlIgnore]
        public MotorBrake SbcLock
        {

            get { return _SecBrkControlLock; }
            set
            {
                if (_SecBrkControlLock != value)
                {

                    _SecBrkControlLock = value;
                    //SecondaryBreakControl sbc = Sys.SbcList.Find(qq => qq.CanChannel == CanChannel);
                    //Sys.printStack(this);
                    sbc.Output = SetBitInByteArray2(sbc.Output, SbcBit, value);
                    OnNotify("SbcLock");

                    //if (MotorID == 1)
                    //Debug.WriteLine(string.Format("{0:hh:mm:ss.fff} MOTOR: {1}, SBC getset:{2}", DateTime.Now, MotorID, SbcLock));

                }
            }
        }

        DateTime _timestamp = DateTime.Now;
        [XmlIgnore]
        public DateTime SetTimestamp { get { return _timestamp; } set { _timestamp = value; } } //za sest sekundi stop

        //public bool CounterForBrakeState = false;

        [XmlIgnore]
        public states state
        {
            get { return _state; }
            set
            {
                if (_state != value)
                {
                    _state = value; OnNotify("state");
                    Log.WriteDetailed(string.Format("motor={0} state={1,10} sbc={2,10} cp={3,10} cv={4,10}", MotorID, state, SbcLock, CP, CV));

                    //Debug.WriteLine("state change motor={0} state={1} cp={2} cpold={3} sbc={4} sbcCanMsgId={5}", MotorID, state, CP, prevCP, SbcLock, Convert.ToString(sbc.PDO_CanMsgID));
                    //Sys.printStack(this);
                }
            }
        }
        states _state = states.Idle;

        //[XmlIgnore]
        //public Sys sys;

        public MotorLogic()
        {
            iMotorIndicators = this;
            iMv = this;
            iSpSv = this;
            calculatesEnabledType = this;


            imotTotalLogic.Names = "I Total :";
            // wtf??? 201102
            //MainWindow.mainWindowDispacher.BeginInvoke(DispatcherPriority.Normal, (Action)delegate()
            //               {
            //                   SumsPPsPanel.Instance.AddPanel(new CountersSumsPPS(imotTotalLogic), 8);
            //               });
        }



        public void SetMotorDirection()
        {
            this.PosDirection = (this.Inverted ? CanSett.CanCommandManNegative : CanSett.CanCommandManPositive);
            this.NegDirection = (this.Inverted ? CanSett.CanCommandManPositive : CanSett.CanCommandManNegative);
        }

        [XmlIgnore]
        public List<DiagItemValues> DiagItemsList = new List<DiagItemValues>();

        public void UpdateImot(byte[] data, int index, double value)
        {
            if (index != CanSett.ImotIndex) return;
            this.Imot = value / 10000;

            foreach (DiagItemValues div in Sys.StaticClusterElectricList)
                div.Value = Sys.StaticMotorList.FindAll(q => q.ClusterElectric == div.Cluster).Sum(t => t.Imot);


            //CountersLogic.UniqueInstance.ImotTotal = Sys.StaticMotorList.Sum(t => t.Imot);
            imotTotalLogic.Count = (int)Sys.StaticMotorList.Sum(t => t.Imot);
            ImotLogic.UniqueInstance.ImotTotal = Sys.StaticMotorList.Sum(t => t.Imot);
        }

        public void UpdateDiagItem(int index, byte subindex, double value)
        {
            DiagItemValues di = DiagItemsList.Find(ttt => (ttt.LenzeIndex == index && ttt.LenzeSubIndex == subindex));
            if (di != null)
                di.Value = value;
        }

        public void FillDiagItems()
        {
            string[] t1 = DiagItems.Split(';');
            foreach (string s in t1)
            {
                string[] t2 = s.Split('/');

                DiagItemsList.Add(new DiagItemValues()
                {
                    LenzeIndex = System.Convert.ToInt32(t2[1]),
                    LenzeSubIndex = System.Convert.ToByte(t2[2]),
                    Text = t2[0],
                    Units = t2[3]
                });
            }
        }

        public void ResetTripHistory9400()
        {
            TripHistoryList.Clear();
            UdpTx.UniqueInstance.SendSDOReadRQ(MotorID, 168, 0, SdoPriority.lo);

            //for (byte i = 1; i < 9; i++)
            //{
            //    UdpTx.UniqueInstance.SendSDOWriteRQ_Raw(MotorID, 171, 0, i, SdoPriority.hi);
            //    //UdpTx.UniqueInstance.SendSDOWriteRQ(MotorID, 178, i, SdoPriority.lo);
            //    UdpTx.UniqueInstance.SendSDOReadRQ(MotorID, 175, 0, SdoPriority.lo);//error
            //    UdpTx.UniqueInstance.SendSDOReadRQ(MotorID, 176, 0, SdoPriority.lo);//internal clock
            //    UdpTx.UniqueInstance.SendSDOReadRQ(MotorID, 177, 0, SdoPriority.lo);//poweron meter
            //}



        }
        //public void ResetTripHistory9300() //20140516 nema na 9400, uklonjeno
        //{
        //    TripHistoryList.Clear();

        //    for (byte i = 1; i < 9; i++)
        //    {
        //        UdpTx.UniqueInstance.SendSDOReadRQ(MotorID, 168, i, SdoPriority.lo);
        //        UdpTx.UniqueInstance.SendSDOReadRQ(MotorID, 169, i, SdoPriority.lo);
        //        UdpTx.UniqueInstance.SendSDOReadRQ(MotorID, 170, i, SdoPriority.lo);
        //    }
        //}

        public void ReceiveTripHistoryData9400(byte[] data, int index, byte subindex, int value)
        {
            if (!(index == 168 && subindex == 0)) return;

            //int TripID = BitConverter.ToInt16(data, 6);
            int TripID = BitConverter.ToInt32(data, 4) & 16777215;

            TripXML9400 tempTrip = Sys.StaticListOfTripsLenze9400.Find(t => t.Number == TripID);
            if (tempTrip == null) return;

            TripHistoryList.Add(new TripHistoryItem() { Error = tempTrip.Number.ToString(), ErrorText = tempTrip.Value });
        }
        //public void ReceiveTripHistoryData9300(byte[] data, int index, byte subindex, int value)
        //{
        //    if (!(index == 168 || index == 169 || index == 170)) return;


        //    int TripID = ((((value / 10000) % 3000) % 2000) % 1000);

        //    while (TripHistoryList.Count < subindex) TripHistoryList.Add(new TripHistoryItem());

        //    TripHistoryItem temp = TripHistoryList[subindex - 1];
        //    TripXML9300 tempTrip = Sys.StaticListOfTripsLenze9300.Find(t => t.ID == TripID);
        //    if (tempTrip == null) return;//todo 20140512

        //    switch (index)
        //    {
        //        case 168:
        //            {
        //                temp.Error = tempTrip.Error;
        //                temp.ErrorCauseRemedy = tempTrip.CauseRemedy;
        //                temp.ErrorText = tempTrip.ErrorDescription;
        //                break;
        //            }
        //        case 169: temp.Time = value; break;
        //        case 170: temp.Count = value / 10000; break;
        //    }
        //}

        public List<MotorLogic> toList()
        {
            List<MotorLogic> a = new List<MotorLogic>();
            a.Add(this);
            return a;
        }

        //public void CalculateButtonsAndIndicatorsIsEnabled(MotorLogic motor1)
        public void CalculateEnableds()
        {
            autoEnabled = manEnabled = manPosNegEnabled = MVEnabled = SpSvEnabled = true;
            GroupChangeEnabled = true;
            CanOffVisibility = Visibility.Visible;

            //darko promenio murska sobota 20101201 (cim stavis motor u grupu on pokaze CANOFF)
            if (!Can)
                CanOffVisibility = Visibility.Visible;
            else
                CanOffVisibility = Visibility.Collapsed;


            if (GroupNo == -1)
            {


                autoEnabled = (Can && RefOk && !Moving && !Trip && Brake && !Local && manEnabled && ApplicationStateLogic.Instance.PilzActive);
                stopEnabled = !Local;
                GroupChangeEnabled = (!Local && Brake);
                SpSvEnabled = MVEnabled = manEnabled = (Can && !Moving && !Trip && !Local && Brake && ApplicationStateLogic.Instance.PilzActive);
                manPosNegEnabled = (state == states.ManStarted2);



            }
            else
                autoEnabled = manEnabled = manPosNegEnabled = MVEnabled = SpSvEnabled = false;

            //if(Sys.lockMotors.OverrideActive&&Sys.LocksEnabled&&Sys.LocksOverrideEnabled) 
            if (this.Lock && Sys.LocksEnabled && (Sys.LocksOverrideEnabled && !Sys.lockMotors.OverrideActive))
            { autoEnabled = manEnabled = manPosNegEnabled = false; }

        }

        public static byte[] SetBitInByteArray2(byte[] _in, int pos, MotorBrake value)
        {
            byte[] ttt = new byte[8];
            Buffer.BlockCopy(_in, 0, ttt, 0, 8);
            if (value == MotorBrake.Unlocked) //true
                ttt[Convert.ToInt32(pos / 8)] = (byte)(ttt[Convert.ToInt32(pos / 8)] | (byte)(1 << ((pos % 8))));
            else
                ttt[Convert.ToInt32(pos / 8)] = (byte)(ttt[Convert.ToInt32(pos / 8)] & (byte)~(1 << ((pos % 8))));
            return ttt;
        }

        //public static byte[] SetBitInByteArray2(byte[] _in, int pos, MotorBrake value)
        //{
        //    if (value == MotorBrake.Unlocked) //true
        //        _in[Convert.ToInt32(pos / 8)] = (byte)(_in[Convert.ToInt32(pos / 8)] | (byte)(1 << ((pos % 8))));
        //    else
        //        _in[Convert.ToInt32(pos / 8)] = (byte)(_in[Convert.ToInt32(pos / 8)] & (byte)~(1 << ((pos % 8))));
        //    return _in;
        //}
        //public static byte[] SetBitInByteArray2(byte[] _in, int pos, MotorBrake value)
        //{
        //    if (value == MotorBrake.Unlocked) //true
        //        _in[Convert.ToInt32(pos / 8)] = (byte)(_in[Convert.ToInt32(pos / 8)] | (byte)(1 << ((pos % 8) - 1)));
        //    else
        //        _in[Convert.ToInt32(pos / 8)] = (byte)(_in[Convert.ToInt32(pos / 8)] & (byte)~(1 << ((pos % 8) - 1)));
        //    return _in;
        //}

        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnNotify(String parameter)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(parameter));
            }
        }
        private void LogAndNotify(String parameter, bool value) // proveriti
        {
            //Log.Write("Motor " + MotorID.ToString() + ": " + parameter + " changed to " + value.ToString());
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(parameter));
            }
        }

        #endregion

        //#region ImotorState Members

        //public void GroupMovemenetStatus(MotorLogic motor1) { }

        //#endregion


        #region ILinkCheck
        [XmlIgnore]
        public bool LinkIndicator { get { return this.Can; } set { } }//this.can radi notify

        public void PingDevice() { }//plc pinguje

        public void ResponseReceived() { }//can rx radi

        public void ResetIndicator() { }//can rx radi


        #endregion
    }
}
