; Script generated by the Inno Setup Script Wizard.
; SEE THE DOCUMENTATION FOR DETAILS ON CREATING INNO SETUP SCRIPT FILES!

#define MyAppName "StClientDr"
#define MyAppVersion "4.14.2.1"
#define MyAppPublisher "Svetlost teatar"
#define MyAppURL "www.steatar.rs"
#define MyAppExeName "StClient.exe"

[Setup]
; NOTE: The value of AppId uniquely identifies this application.
; Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)
AppId={{A1354184-FA93-4945-B4EE-F7913C3AB7E7}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
;AppVerName={#MyAppName} {#MyAppVersion}
AppPublisher={#MyAppPublisher}
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
AppUpdatesURL={#MyAppURL}
DefaultDirName={pf}\StClient
DisableDirPage=yes
DefaultGroupName={#MyAppName}
DisableProgramGroupPage=yes
OutputDir=C:\st\20140509\StClientSetupInno
OutputBaseFilename=setup
SetupIconFile=C:\st\20140509\StClient\ikona client 000.ico
Compression=lzma
SolidCompression=yes

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked
Name: "quicklaunchicon"; Description: "{cm:CreateQuickLaunchIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked; OnlyBelowVersion: 0,6.1

[Files]
Source: "C:\st\20140509\StClient\bin\Debug\StClient.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\st\20140509\StClient\bin\Debug\Cues\*"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "C:\Cues\*"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "C:\st\20140509\StClient\bin\Debug\Backup\*"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\st\20140509\StClient\bin\Debug\Properties\*"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "C:\st\20140509\StClient\bin\Debug\LogAlertHB.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\st\20140509\StClient\bin\Debug\LogAlertHB.pdb"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\st\20140509\StClient\bin\Debug\phidget21.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\st\20140509\StClient\bin\Debug\Phidget21.NET.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\st\20140509\StClient\bin\Debug\StClient.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\st\20140509\StClient\bin\Debug\StClient.exe.config"; DestDir: "{app}"; Flags: ignoreversion
; NOTE: Don't use "Flags: ignoreversion" on any shared system files

[Icons]
Name: "{group}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"
Name: "{commondesktop}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; Tasks: desktopicon
Name: "{userappdata}\Microsoft\Internet Explorer\Quick Launch\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; Tasks: quicklaunchicon

[Run]
Filename: "{app}\{#MyAppExeName}"; Description: "{cm:LaunchProgram,{#StringChange(MyAppName, '&', '&&')}}"; Flags: nowait postinstall skipifsilent
