﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using StConfig;

namespace StClient
{
    /// <summary>
    /// Interaction logic for SystemStates2.xaml
    /// </summary>
    public partial class ViewSelect : UserControl
    {
        Properties.Settings Sett = Properties.Settings.Default;
        Properties.CanSettings CanSett = Properties.CanSettings.Default;
        List<ToggleButton> tbList = new List<ToggleButton>();

        public ViewSelect()
        {
            InitializeComponent();
            this.DataContext = VisibilityStatesLogic.UniqueInstance;
            tbList.Add(PB1); tbList.Add(PB2); tbList.Add(PB3); tbList.Add(PB4); tbList.Add(PB5); tbList.Add(PB6); tbList.Add(PB7);
            foreach (ToggleButton tb in tbList) tb.Visibility = Visibility.Hidden;

            TimedAction.ExecuteWithDelay(new Action(delegate
            {
                foreach (StClusters stc in Sys.ReadClusterList)
                {
                    tbList[stc.clusterID-1].Visibility = stc.used? Visibility.Visible: Visibility.Hidden;
                    tbList[stc.clusterID-1].Content = stc.clusterName;
                }
            }), TimeSpan.FromSeconds(4));
        }




        private void viewAll_Click(object sender, RoutedEventArgs e)
        {
            VisibilityStatesLogic.UniqueInstance.ShowAllShowNone(true);
        }

        private void viewNone_Click(object sender, RoutedEventArgs e)
        {
            VisibilityStatesLogic.UniqueInstance.ShowAllShowNone(false);
        }

        private void viewCued_Click(object sender, RoutedEventArgs e)
        {
            VisibilityStatesLogic.UniqueInstance.ShowCuedMotors();

        }

        private void viewMoving_Click(object sender, RoutedEventArgs e)
        {
            VisibilityStatesLogic.UniqueInstance.ShowMovingMotors();
        }

        private void viewTripped_Click(object sender, RoutedEventArgs e)
        {
            VisibilityStatesLogic.UniqueInstance.ShowTrippedMotors();
        }

        private void LogOff_Click(object sender, RoutedEventArgs e)
        {

           // ApplicationStateLogic.Instance.isAppEnabled = false;

            List<MotorLogic> MovingMotorsList = Sys.StaticMotorList.FindAll(t => t.Can == true && t.Brake == false);

            foreach (MotorLogic motor in MovingMotorsList)
            {
                UdpTx.UniqueInstance.SendPDO(motor.MotorID, CanSett.CanCommandStop, 0, 8);
                //  UdpTx.UniqueInstance.SendPdoSbc((byte)motor.CanChannel, (byte)motor.SecBrkControlAddress, 0);
                UdpTx.UniqueInstance.SendPDO(motor.MotorID, CanSett.CanCommandIdle, 50, 8);
            }
        }

        List<int> clusterList = new List<int>();


        private void Button_Click(object sender, RoutedEventArgs e)
        {
            App.Current.Shutdown();
        }


        private void one_Click(object sender, RoutedEventArgs e)
        {
            VisibilityStatesLogic.UniqueInstance.ClusterListAddRemove(1, false);
        }

        private void two_Click(object sender, RoutedEventArgs e)
        {
            VisibilityStatesLogic.UniqueInstance.ClusterListAddRemove(2, false);
        }

        private void three_Click(object sender, RoutedEventArgs e)
        {
            VisibilityStatesLogic.UniqueInstance.ClusterListAddRemove(3, false);
        }

        private void four_Click(object sender, RoutedEventArgs e)
        {
            VisibilityStatesLogic.UniqueInstance.ClusterListAddRemove(4, false);
        }

        private void five_Click(object sender, RoutedEventArgs e)
        {
            VisibilityStatesLogic.UniqueInstance.ClusterListAddRemove(5, false);
        }
        private void six_Click(object sender, RoutedEventArgs e)
        {
            VisibilityStatesLogic.UniqueInstance.ClusterListAddRemove(6, false);
        }

        private void seven_Click(object sender, RoutedEventArgs e)
        {
            VisibilityStatesLogic.UniqueInstance.ClusterListAddRemove(7, false);
        }

        private void qqq_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.testWindow = new TestWindow000();
            MainWindow.testWindow.Show();
        }
    }
}
