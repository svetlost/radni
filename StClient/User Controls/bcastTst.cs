﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Text.RegularExpressions;

namespace StClient
{
    /// <summary>
    /// Interaction logic for bcastTst.xaml
    /// </summary>
    public partial class bcastTst : Window, INotifyPropertyChanged
    {
        public string ReturnValue { get; set; }
        public string CurrentValue { get; set; }
        double numberToProcess;
        private bool _spenabled, isStringType;
        public bool IsStringType { get { return isStringType; } set { isStringType = value; } }
        public bool SpSvEnabled { get { return _spenabled; } set { if (_spenabled != value) { _spenabled = value; OnNotify("SpSvEnabled"); } } }
        public double LimitMin { get; set; }
        public double LimitMax { get; set; }

        public string ValueDescription { get; set; }
        TextBlock tblock;

        //bool PopupAlreadyOpen() { return (App.Current.Windows.Count > 2); }

        public bcastTst(TextBlock _sender, bool isString, double X, double Y)
        {
            try
            {
                //if (App.Current.Windows.Count > 2) this.Close();
                InitializeComponent();

                this.DataContext = this;
                tb_TextChanged(popuptextbox, null);

                LimitMax = 100000;
                LimitMin = -100000;
                tblock = _sender;
                this.ValueDescription = tblock.Name; //promene 16 jun:
                this.CurrentValue = tblock.Text;
                this.Title = string.Format("Enter value for {0}.", ValueDescription);
                IsStringType = isString;
                Sys.receivedTxtBlock = popuptextbox;
                this.Left = X;
                this.Top = Y;

                this.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

       

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            double t = 0;
            if (!IsStringType)
            {

                if (double.TryParse(popuptextbox.Text, out t))
                {
                    t = (t > LimitMax ? LimitMax : t);
                    t = (t < LimitMin ? LimitMin : t);
                }
            }

            if (popuptextbox.Text == "")
                tblock.Text = CurrentValue;
            else
                if (!IsStringType)
                {
                    tblock.Text = string.Format("{0:0.0}", t);
                }
                else
                    tblock.Text = popuptextbox.Text;

            DialogResult = true;
            this.Close();

        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void Window_Activated(object sender, EventArgs e)
        {
            switch (ValueDescription) //promene 16 jun: sve ovo je novo
            {
                case "Copy":
                    tbTitle.Text = string.Format("Copy current cue to desired location");
                    textBlock1.Text = "Copy To:";
                    break;
                case "Description":
                    tbTitle.Text = string.Format("Motor Description");
                    textBlock1.Text = "Description:";
                    break;
                case "Electric":
                    tbTitle.Text = string.Format("Electric Cluster Number");
                    textBlock1.Text = "Cluster:";
                    break;
                case "Logic":
                    tbTitle.Text = string.Format("Logic Cluster Number");
                    textBlock1.Text = "Cluster:";
                    break;
                case "Cue":
                    LimitMin = 0;
                    LimitMax = 100;
                    tbTitle.Text = string.Format("Enter value for CUE NUMBER, min={0}, max={1}", LimitMin, LimitMax);
                    textBlock1.Text = "CUE No:";
                    break;
                case "Group":
                    LimitMin = -1;
                    LimitMax = Sys.StaticGroupList.Count - 1;
                    tbTitle.Text = string.Format("Enter value for GROUP NUMBER, min={0}, max={1} ; -1 Removes from group", LimitMin, LimitMax);
                    textBlock1.Text = "GROUP No:";
                    break;
                default:
                    tbTitle.Text = string.Format("Enter value for {0}, min={1}, max={2}", ValueDescription, LimitMin, LimitMax);
                    textBlock1.Text = ValueDescription;
                    break;
            }



            popuptextbox.Text = tblock.Text;
            popuptextbox.Focus();
            popuptextbox.SelectAll();
        }

        #region Provera
        //proveri svaki unos - dozvoljava brojeve
        private void tb_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (IsStringType) return;
            else if (e.Key == Key.Left || e.Key == Key.Home || e.Key == Key.Delete || e.Key == Key.Enter || e.Key == Key.Escape || e.Key == Key.Right || e.Key == Key.Back || ValidateTextInput(e.Key.ToString())) { return; }
            else if (e.Key == Key.Space) { e.Handled = true; return; }
            else if (e.Key == Key.Decimal) { if (((TextBox)sender).Text.Contains(".")) { e.Handled = true; return; } }
            else if (e.Key == Key.Subtract) { if (((TextBox)sender).Text.Contains("-") || ((TextBox)sender).CaretIndex != 0) { e.Handled = true; return; } }
            else { e.Handled = true; }
        }

        private void tb_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (IsStringType) { SpSvEnabled = true; return; }
            if (!ValidateTextInput(((TextBox)sender).Text)) { SpSvEnabled = false; return; }
            if (((TextBox)sender).Text.Contains("-.")) { SpSvEnabled = false; return; }
            try
            {
                double.TryParse(((TextBox)sender).Text, out numberToProcess);
                if (numberToProcess < LimitMin) { SpSvEnabled = false; return; }
                if (numberToProcess > LimitMax) { SpSvEnabled = false; return; }
                else { SpSvEnabled = true; }
            }
            catch (Exception ex) { LogAlertHB.Log.Write(ex.Message); }
        }

        private bool ValidateTextInput(string aTextInput)
        {
            Match lInvalidMatch = Regex.Match(aTextInput, "[0-9]");
            return (lInvalidMatch.Success == true);
        }
        #endregion

        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnNotify(String parameter)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(parameter));
            }
        }
        #endregion

 
      
    }



}
