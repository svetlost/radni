﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StClient
{

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //[System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    //public partial class DeviceDescription
    //{

    //    private DeviceDescriptionDevice deviceField;

    //    private DeviceDescriptionParameterTemplate[] templatesField;

    //    private DeviceDescriptionParameter[] codelistField;

    //    private DeviceDescriptionSubMenu[] codeListMenuField;

    //    private DeviceDescriptionError[] errorListField;

    //    /// <remarks/>
    //    public DeviceDescriptionDevice Device
    //    {
    //        get
    //        {
    //            return this.deviceField;
    //        }
    //        set
    //        {
    //            this.deviceField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlArrayItemAttribute("ParameterTemplate", IsNullable = false)]
    //    public DeviceDescriptionParameterTemplate[] Templates
    //    {
    //        get
    //        {
    //            return this.templatesField;
    //        }
    //        set
    //        {
    //            this.templatesField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlArrayItemAttribute("Parameter", IsNullable = false)]
    //    public DeviceDescriptionParameter[] Codelist
    //    {
    //        get
    //        {
    //            return this.codelistField;
    //        }
    //        set
    //        {
    //            this.codelistField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlArrayItemAttribute("SubMenu", IsNullable = false)]
    //    public DeviceDescriptionSubMenu[] CodeListMenu
    //    {
    //        get
    //        {
    //            return this.codeListMenuField;
    //        }
    //        set
    //        {
    //            this.codeListMenuField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlArrayItemAttribute("Error", IsNullable = false)]
    //    public DeviceDescriptionError[] ErrorList
    //    {
    //        get
    //        {
    //            return this.errorListField;
    //        }
    //        set
    //        {
    //            this.errorListField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class DeviceDescriptionDevice
    //{

    //    private string nameField;

    //    private byte externalReleaseNumberField;

    //    private byte externalLevelNumberField;

    //    private byte internalReleaseNumberField;

    //    private byte internalLevelNumberField;

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public string Name
    //    {
    //        get
    //        {
    //            return this.nameField;
    //        }
    //        set
    //        {
    //            this.nameField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public byte ExternalReleaseNumber
    //    {
    //        get
    //        {
    //            return this.externalReleaseNumberField;
    //        }
    //        set
    //        {
    //            this.externalReleaseNumberField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public byte ExternalLevelNumber
    //    {
    //        get
    //        {
    //            return this.externalLevelNumberField;
    //        }
    //        set
    //        {
    //            this.externalLevelNumberField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public byte InternalReleaseNumber
    //    {
    //        get
    //        {
    //            return this.internalReleaseNumberField;
    //        }
    //        set
    //        {
    //            this.internalReleaseNumberField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public byte InternalLevelNumber
    //    {
    //        get
    //        {
    //            return this.internalLevelNumberField;
    //        }
    //        set
    //        {
    //            this.internalLevelNumberField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class DeviceDescriptionParameterTemplate
    //{

    //    private DeviceDescriptionParameterTemplateDisplayFactor displayFactorField;

    //    private DeviceDescriptionParameterTemplateStep stepField;

    //    private DeviceDescriptionParameterTemplateAttributes attributesField;

    //    private string nameField;

    //    /// <remarks/>
    //    public DeviceDescriptionParameterTemplateDisplayFactor DisplayFactor
    //    {
    //        get
    //        {
    //            return this.displayFactorField;
    //        }
    //        set
    //        {
    //            this.displayFactorField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public DeviceDescriptionParameterTemplateStep Step
    //    {
    //        get
    //        {
    //            return this.stepField;
    //        }
    //        set
    //        {
    //            this.stepField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public DeviceDescriptionParameterTemplateAttributes Attributes
    //    {
    //        get
    //        {
    //            return this.attributesField;
    //        }
    //        set
    //        {
    //            this.attributesField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public string Name
    //    {
    //        get
    //        {
    //            return this.nameField;
    //        }
    //        set
    //        {
    //            this.nameField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class DeviceDescriptionParameterTemplateDisplayFactor
    //{

    //    private byte nominatorField;

    //    private byte denominatorField;

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public byte Nominator
    //    {
    //        get
    //        {
    //            return this.nominatorField;
    //        }
    //        set
    //        {
    //            this.nominatorField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public byte Denominator
    //    {
    //        get
    //        {
    //            return this.denominatorField;
    //        }
    //        set
    //        {
    //            this.denominatorField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class DeviceDescriptionParameterTemplateStep
    //{

    //    private byte valueField;

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public byte Value
    //    {
    //        get
    //        {
    //            return this.valueField;
    //        }
    //        set
    //        {
    //            this.valueField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class DeviceDescriptionParameterTemplateAttributes
    //{

    //    private bool accessReadField;

    //    private bool accessWriteField;

    //    private bool cinhDependendField;

    //    private bool plcStopDependendField;

    //    private bool noTransferField;

    //    private bool invisibleField;

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public bool AccessRead
    //    {
    //        get
    //        {
    //            return this.accessReadField;
    //        }
    //        set
    //        {
    //            this.accessReadField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public bool AccessWrite
    //    {
    //        get
    //        {
    //            return this.accessWriteField;
    //        }
    //        set
    //        {
    //            this.accessWriteField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public bool CinhDependend
    //    {
    //        get
    //        {
    //            return this.cinhDependendField;
    //        }
    //        set
    //        {
    //            this.cinhDependendField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public bool PlcStopDependend
    //    {
    //        get
    //        {
    //            return this.plcStopDependendField;
    //        }
    //        set
    //        {
    //            this.plcStopDependendField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public bool NoTransfer
    //    {
    //        get
    //        {
    //            return this.noTransferField;
    //        }
    //        set
    //        {
    //            this.noTransferField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public bool Invisible
    //    {
    //        get
    //        {
    //            return this.invisibleField;
    //        }
    //        set
    //        {
    //            this.invisibleField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class DeviceDescriptionParameter
    //{

    //    private object[] itemsField;

    //    private ushort codeNumberField;

    //    private string parameterTypeField;

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlElementAttribute("Attributes", typeof(DeviceDescriptionParameterAttributes))]
    //    [System.Xml.Serialization.XmlElementAttribute("BitFieldText", typeof(DeviceDescriptionParameterBitFieldText))]
    //    [System.Xml.Serialization.XmlElementAttribute("DataType", typeof(DeviceDescriptionParameterDataType))]
    //    [System.Xml.Serialization.XmlElementAttribute("DefaultValues", typeof(DeviceDescriptionParameterDefaultValues))]
    //    [System.Xml.Serialization.XmlElementAttribute("DisplayFactor", typeof(DeviceDescriptionParameterDisplayFactor))]
    //    [System.Xml.Serialization.XmlElementAttribute("Limits", typeof(DeviceDescriptionParameterLimits))]
    //    [System.Xml.Serialization.XmlElementAttribute("ParameterName", typeof(DeviceDescriptionParameterParameterName))]
    //    [System.Xml.Serialization.XmlElementAttribute("SelectList", typeof(DeviceDescriptionParameterSelectList))]
    //    [System.Xml.Serialization.XmlElementAttribute("Step", typeof(DeviceDescriptionParameterStep))]
    //    [System.Xml.Serialization.XmlElementAttribute("Subcode", typeof(DeviceDescriptionParameterSubcode))]
    //    [System.Xml.Serialization.XmlElementAttribute("SubcodeDescription", typeof(DeviceDescriptionParameterSubcodeDescription))]
    //    [System.Xml.Serialization.XmlElementAttribute("Template", typeof(DeviceDescriptionParameterTemplate1))]
    //    [System.Xml.Serialization.XmlElementAttribute("Unit", typeof(DeviceDescriptionParameterUnit))]
    //    public object[] Items
    //    {
    //        get
    //        {
    //            return this.itemsField;
    //        }
    //        set
    //        {
    //            this.itemsField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public ushort CodeNumber
    //    {
    //        get
    //        {
    //            return this.codeNumberField;
    //        }
    //        set
    //        {
    //            this.codeNumberField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public string ParameterType
    //    {
    //        get
    //        {
    //            return this.parameterTypeField;
    //        }
    //        set
    //        {
    //            this.parameterTypeField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class DeviceDescriptionParameterAttributes
    //{

    //    private bool accessReadField;

    //    private bool accessWriteField;

    //    private bool cinhDependendField;

    //    private bool plcStopDependendField;

    //    private bool noTransferField;

    //    private bool invisibleField;

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public bool AccessRead
    //    {
    //        get
    //        {
    //            return this.accessReadField;
    //        }
    //        set
    //        {
    //            this.accessReadField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public bool AccessWrite
    //    {
    //        get
    //        {
    //            return this.accessWriteField;
    //        }
    //        set
    //        {
    //            this.accessWriteField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public bool CinhDependend
    //    {
    //        get
    //        {
    //            return this.cinhDependendField;
    //        }
    //        set
    //        {
    //            this.cinhDependendField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public bool PlcStopDependend
    //    {
    //        get
    //        {
    //            return this.plcStopDependendField;
    //        }
    //        set
    //        {
    //            this.plcStopDependendField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public bool NoTransfer
    //    {
    //        get
    //        {
    //            return this.noTransferField;
    //        }
    //        set
    //        {
    //            this.noTransferField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public bool Invisible
    //    {
    //        get
    //        {
    //            return this.invisibleField;
    //        }
    //        set
    //        {
    //            this.invisibleField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class DeviceDescriptionParameterBitFieldText
    //{

    //    private DeviceDescriptionParameterBitFieldTextBitText[] bitTextField;

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlElementAttribute("BitText")]
    //    public DeviceDescriptionParameterBitFieldTextBitText[] BitText
    //    {
    //        get
    //        {
    //            return this.bitTextField;
    //        }
    //        set
    //        {
    //            this.bitTextField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class DeviceDescriptionParameterBitFieldTextBitText
    //{

    //    private byte bitPosField;

    //    private string rIDField;

    //    private string valueField;

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public byte BitPos
    //    {
    //        get
    //        {
    //            return this.bitPosField;
    //        }
    //        set
    //        {
    //            this.bitPosField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public string RID
    //    {
    //        get
    //        {
    //            return this.rIDField;
    //        }
    //        set
    //        {
    //            this.rIDField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlTextAttribute()]
    //    public string Value
    //    {
    //        get
    //        {
    //            return this.valueField;
    //        }
    //        set
    //        {
    //            this.valueField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class DeviceDescriptionParameterDataType
    //{

    //    private string typeField;

    //    private ushort dataByteLengthField;

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public string Type
    //    {
    //        get
    //        {
    //            return this.typeField;
    //        }
    //        set
    //        {
    //            this.typeField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public ushort DataByteLength
    //    {
    //        get
    //        {
    //            return this.dataByteLengthField;
    //        }
    //        set
    //        {
    //            this.dataByteLengthField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class DeviceDescriptionParameterDefaultValues
    //{

    //    private DeviceDescriptionParameterDefaultValuesValue[] valueField;

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlElementAttribute("Value")]
    //    public DeviceDescriptionParameterDefaultValuesValue[] Value
    //    {
    //        get
    //        {
    //            return this.valueField;
    //        }
    //        set
    //        {
    //            this.valueField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class DeviceDescriptionParameterDefaultValuesValue
    //{

    //    private byte subcodeField;

    //    private string valueField;

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public byte Subcode
    //    {
    //        get
    //        {
    //            return this.subcodeField;
    //        }
    //        set
    //        {
    //            this.subcodeField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlTextAttribute()]
    //    public string Value
    //    {
    //        get
    //        {
    //            return this.valueField;
    //        }
    //        set
    //        {
    //            this.valueField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class DeviceDescriptionParameterDisplayFactor
    //{

    //    private byte nominatorField;

    //    private uint denominatorField;

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public byte Nominator
    //    {
    //        get
    //        {
    //            return this.nominatorField;
    //        }
    //        set
    //        {
    //            this.nominatorField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public uint Denominator
    //    {
    //        get
    //        {
    //            return this.denominatorField;
    //        }
    //        set
    //        {
    //            this.denominatorField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class DeviceDescriptionParameterLimits
    //{

    //    private decimal lowLimitField;

    //    private decimal highLimitField;

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public decimal LowLimit
    //    {
    //        get
    //        {
    //            return this.lowLimitField;
    //        }
    //        set
    //        {
    //            this.lowLimitField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public decimal HighLimit
    //    {
    //        get
    //        {
    //            return this.highLimitField;
    //        }
    //        set
    //        {
    //            this.highLimitField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class DeviceDescriptionParameterParameterName
    //{

    //    private string rIDField;

    //    private string valueField;

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public string RID
    //    {
    //        get
    //        {
    //            return this.rIDField;
    //        }
    //        set
    //        {
    //            this.rIDField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlTextAttribute()]
    //    public string Value
    //    {
    //        get
    //        {
    //            return this.valueField;
    //        }
    //        set
    //        {
    //            this.valueField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class DeviceDescriptionParameterSelectList
    //{

    //    private DeviceDescriptionParameterSelectListSelectionText[] selectionTextField;

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlElementAttribute("SelectionText")]
    //    public DeviceDescriptionParameterSelectListSelectionText[] SelectionText
    //    {
    //        get
    //        {
    //            return this.selectionTextField;
    //        }
    //        set
    //        {
    //            this.selectionTextField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class DeviceDescriptionParameterSelectListSelectionText
    //{

    //    private uint valueField;

    //    private string rIDField;

    //    private string value1Field;

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public uint Value
    //    {
    //        get
    //        {
    //            return this.valueField;
    //        }
    //        set
    //        {
    //            this.valueField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public string RID
    //    {
    //        get
    //        {
    //            return this.rIDField;
    //        }
    //        set
    //        {
    //            this.rIDField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlTextAttribute()]
    //    public string Value1
    //    {
    //        get
    //        {
    //            return this.value1Field;
    //        }
    //        set
    //        {
    //            this.value1Field = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class DeviceDescriptionParameterStep
    //{

    //    private float valueField;

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public float Value
    //    {
    //        get
    //        {
    //            return this.valueField;
    //        }
    //        set
    //        {
    //            this.valueField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class DeviceDescriptionParameterSubcode
    //{

    //    private byte numberField;

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public byte Number
    //    {
    //        get
    //        {
    //            return this.numberField;
    //        }
    //        set
    //        {
    //            this.numberField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class DeviceDescriptionParameterSubcodeDescription
    //{

    //    private DeviceDescriptionParameterSubcodeDescriptionSubcodeText[] subcodeTextField;

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlElementAttribute("SubcodeText")]
    //    public DeviceDescriptionParameterSubcodeDescriptionSubcodeText[] SubcodeText
    //    {
    //        get
    //        {
    //            return this.subcodeTextField;
    //        }
    //        set
    //        {
    //            this.subcodeTextField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class DeviceDescriptionParameterSubcodeDescriptionSubcodeText
    //{

    //    private byte subcodeField;

    //    private string rIDField;

    //    private string valueField;

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public byte Subcode
    //    {
    //        get
    //        {
    //            return this.subcodeField;
    //        }
    //        set
    //        {
    //            this.subcodeField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public string RID
    //    {
    //        get
    //        {
    //            return this.rIDField;
    //        }
    //        set
    //        {
    //            this.rIDField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlTextAttribute()]
    //    public string Value
    //    {
    //        get
    //        {
    //            return this.valueField;
    //        }
    //        set
    //        {
    //            this.valueField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class DeviceDescriptionParameterTemplate1
    //{

    //    private string nameField;

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public string Name
    //    {
    //        get
    //        {
    //            return this.nameField;
    //        }
    //        set
    //        {
    //            this.nameField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class DeviceDescriptionParameterUnit
    //{

    //    private string rIDField;

    //    private string valueField;

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public string RID
    //    {
    //        get
    //        {
    //            return this.rIDField;
    //        }
    //        set
    //        {
    //            this.rIDField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlTextAttribute()]
    //    public string Value
    //    {
    //        get
    //        {
    //            return this.valueField;
    //        }
    //        set
    //        {
    //            this.valueField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class DeviceDescriptionSubMenu
    //{

    //    private object[] itemsField;

    //    private ushort indexField;

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlElementAttribute("DeviceService", typeof(DeviceDescriptionSubMenuDeviceService))]
    //    [System.Xml.Serialization.XmlElementAttribute("KeypadParameter", typeof(DeviceDescriptionSubMenuKeypadParameter))]
    //    [System.Xml.Serialization.XmlElementAttribute("Name", typeof(DeviceDescriptionSubMenuName))]
    //    [System.Xml.Serialization.XmlElementAttribute("Parameter", typeof(DeviceDescriptionSubMenuParameter))]
    //    [System.Xml.Serialization.XmlElementAttribute("ParameterRange", typeof(DeviceDescriptionSubMenuParameterRange))]
    //    [System.Xml.Serialization.XmlElementAttribute("SubMenu", typeof(DeviceDescriptionSubMenuSubMenu))]
    //    public object[] Items
    //    {
    //        get
    //        {
    //            return this.itemsField;
    //        }
    //        set
    //        {
    //            this.itemsField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public ushort Index
    //    {
    //        get
    //        {
    //            return this.indexField;
    //        }
    //        set
    //        {
    //            this.indexField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class DeviceDescriptionSubMenuDeviceService
    //{

    //    private DeviceDescriptionSubMenuDeviceServiceName nameField;

    //    private byte numberField;

    //    /// <remarks/>
    //    public DeviceDescriptionSubMenuDeviceServiceName Name
    //    {
    //        get
    //        {
    //            return this.nameField;
    //        }
    //        set
    //        {
    //            this.nameField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public byte Number
    //    {
    //        get
    //        {
    //            return this.numberField;
    //        }
    //        set
    //        {
    //            this.numberField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class DeviceDescriptionSubMenuDeviceServiceName
    //{

    //    private string rIDField;

    //    private string valueField;

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public string RID
    //    {
    //        get
    //        {
    //            return this.rIDField;
    //        }
    //        set
    //        {
    //            this.rIDField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlTextAttribute()]
    //    public string Value
    //    {
    //        get
    //        {
    //            return this.valueField;
    //        }
    //        set
    //        {
    //            this.valueField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class DeviceDescriptionSubMenuKeypadParameter
    //{

    //    private DeviceDescriptionSubMenuKeypadParameterName nameField;

    //    private DeviceDescriptionSubMenuKeypadParameterSelectionText[] selectListField;

    //    private ushort codeNumberField;

    //    /// <remarks/>
    //    public DeviceDescriptionSubMenuKeypadParameterName Name
    //    {
    //        get
    //        {
    //            return this.nameField;
    //        }
    //        set
    //        {
    //            this.nameField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlArrayItemAttribute("SelectionText", IsNullable = false)]
    //    public DeviceDescriptionSubMenuKeypadParameterSelectionText[] SelectList
    //    {
    //        get
    //        {
    //            return this.selectListField;
    //        }
    //        set
    //        {
    //            this.selectListField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public ushort CodeNumber
    //    {
    //        get
    //        {
    //            return this.codeNumberField;
    //        }
    //        set
    //        {
    //            this.codeNumberField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class DeviceDescriptionSubMenuKeypadParameterName
    //{

    //    private string rIDField;

    //    private string valueField;

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public string RID
    //    {
    //        get
    //        {
    //            return this.rIDField;
    //        }
    //        set
    //        {
    //            this.rIDField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlTextAttribute()]
    //    public string Value
    //    {
    //        get
    //        {
    //            return this.valueField;
    //        }
    //        set
    //        {
    //            this.valueField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class DeviceDescriptionSubMenuKeypadParameterSelectionText
    //{

    //    private byte valueField;

    //    private string rIDField;

    //    private string value1Field;

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public byte Value
    //    {
    //        get
    //        {
    //            return this.valueField;
    //        }
    //        set
    //        {
    //            this.valueField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public string RID
    //    {
    //        get
    //        {
    //            return this.rIDField;
    //        }
    //        set
    //        {
    //            this.rIDField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlTextAttribute()]
    //    public string Value1
    //    {
    //        get
    //        {
    //            return this.value1Field;
    //        }
    //        set
    //        {
    //            this.value1Field = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class DeviceDescriptionSubMenuName
    //{

    //    private string rIDField;

    //    private string valueField;

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public string RID
    //    {
    //        get
    //        {
    //            return this.rIDField;
    //        }
    //        set
    //        {
    //            this.rIDField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlTextAttribute()]
    //    public string Value
    //    {
    //        get
    //        {
    //            return this.valueField;
    //        }
    //        set
    //        {
    //            this.valueField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class DeviceDescriptionSubMenuParameter
    //{

    //    private ushort codeNumberField;

    //    private byte subcodeField;

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public ushort CodeNumber
    //    {
    //        get
    //        {
    //            return this.codeNumberField;
    //        }
    //        set
    //        {
    //            this.codeNumberField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public byte Subcode
    //    {
    //        get
    //        {
    //            return this.subcodeField;
    //        }
    //        set
    //        {
    //            this.subcodeField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class DeviceDescriptionSubMenuParameterRange
    //{

    //    private byte minIndexField;

    //    private uint maxIndexField;

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public byte MinIndex
    //    {
    //        get
    //        {
    //            return this.minIndexField;
    //        }
    //        set
    //        {
    //            this.minIndexField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public uint MaxIndex
    //    {
    //        get
    //        {
    //            return this.maxIndexField;
    //        }
    //        set
    //        {
    //            this.maxIndexField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class DeviceDescriptionSubMenuSubMenu
    //{

    //    private object[] itemsField;

    //    private ushort indexField;

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlElementAttribute("DeviceService", typeof(DeviceDescriptionSubMenuSubMenuDeviceService))]
    //    [System.Xml.Serialization.XmlElementAttribute("Name", typeof(DeviceDescriptionSubMenuSubMenuName))]
    //    [System.Xml.Serialization.XmlElementAttribute("Parameter", typeof(DeviceDescriptionSubMenuSubMenuParameter))]
    //    public object[] Items
    //    {
    //        get
    //        {
    //            return this.itemsField;
    //        }
    //        set
    //        {
    //            this.itemsField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public ushort Index
    //    {
    //        get
    //        {
    //            return this.indexField;
    //        }
    //        set
    //        {
    //            this.indexField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class DeviceDescriptionSubMenuSubMenuDeviceService
    //{

    //    private DeviceDescriptionSubMenuSubMenuDeviceServiceName nameField;

    //    private byte numberField;

    //    /// <remarks/>
    //    public DeviceDescriptionSubMenuSubMenuDeviceServiceName Name
    //    {
    //        get
    //        {
    //            return this.nameField;
    //        }
    //        set
    //        {
    //            this.nameField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public byte Number
    //    {
    //        get
    //        {
    //            return this.numberField;
    //        }
    //        set
    //        {
    //            this.numberField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class DeviceDescriptionSubMenuSubMenuDeviceServiceName
    //{

    //    private string rIDField;

    //    private string valueField;

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public string RID
    //    {
    //        get
    //        {
    //            return this.rIDField;
    //        }
    //        set
    //        {
    //            this.rIDField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlTextAttribute()]
    //    public string Value
    //    {
    //        get
    //        {
    //            return this.valueField;
    //        }
    //        set
    //        {
    //            this.valueField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class DeviceDescriptionSubMenuSubMenuName
    //{

    //    private string rIDField;

    //    private string valueField;

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public string RID
    //    {
    //        get
    //        {
    //            return this.rIDField;
    //        }
    //        set
    //        {
    //            this.rIDField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlTextAttribute()]
    //    public string Value
    //    {
    //        get
    //        {
    //            return this.valueField;
    //        }
    //        set
    //        {
    //            this.valueField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class DeviceDescriptionSubMenuSubMenuParameter
    //{

    //    private ushort codeNumberField;

    //    private byte subcodeField;

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public ushort CodeNumber
    //    {
    //        get
    //        {
    //            return this.codeNumberField;
    //        }
    //        set
    //        {
    //            this.codeNumberField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public byte Subcode
    //    {
    //        get
    //        {
    //            return this.subcodeField;
    //        }
    //        set
    //        {
    //            this.subcodeField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class DeviceDescriptionError
    //{

    //    private uint numberField;

    //    private string rIDField;

    //    private string valueField;

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public uint Number
    //    {
    //        get
    //        {
    //            return this.numberField;
    //        }
    //        set
    //        {
    //            this.numberField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public string RID
    //    {
    //        get
    //        {
    //            return this.rIDField;
    //        }
    //        set
    //        {
    //            this.rIDField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlTextAttribute()]
    //    public string Value
    //    {
    //        get
    //        {
    //            return this.valueField;
    //        }
    //        set
    //        {
    //            this.valueField = value;
    //        }
    //    }
    //}



    class brisiiiClass1
    {
    }
}
