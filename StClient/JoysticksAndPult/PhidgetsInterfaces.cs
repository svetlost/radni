﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Phidgets;         //needed for the interfacekit class and the phidget exception class
using Phidgets.Events;
using System.Windows.Threading;
using System.Windows;
using System.Xml.Serialization;
using System.Reflection;
using LogAlertHB;
using System.Windows.Controls;
using System.Windows.Input;
using System.Diagnostics;
using System.Windows.Data;
using System.Threading;
using StClient.Properties;
using System.ComponentModel;



namespace StClient
{
    /// <summary>
    /// Manager is needed to open all Interface kits
    /// it is the only way it works
    /// phidgets GM=Group or Single Motor;Analog = Joystick with 4 more free inputs  
    /// </summary>
    public class PhidgetsInterfaces : INotifyPropertyChanged
    {
        private RFID rfid; //Declare an RFID object
        //List<RfidItem> rfidList;
        Commands commands = new Commands();


        //XmlUtil<StJoystick> JoystickXml = new XmlUtil<StJoystick>();
        //XmlUtil<RfidItem> rfidXml = new XmlUtil<RfidItem>();
        Manager manager = new Manager();
        InterfaceKit tempIfKit;
        //PhidgetItem tempPhidgetItem;
        //StJoystick phJoystickItem;
        List<MotorLogic> motorsList;
        //public List<StJoystick> joystickList;
        List<InterfaceKit> InterfaceKitList = new List<InterfaceKit>();

        Properties.Settings Sett = Properties.Settings.Default;
        Properties.CanSettings CanSett = Properties.CanSettings.Default;


        //PhidgetItem JoystickPhidget; //da se ne odredjuje svaki put ko je Joystick u senzorima

        ApplicationStateLogic mainWindowEnabled = ApplicationStateLogic.Instance;

        //string _pathConfig = Environment.CurrentDirectory + @"\Config\";
        //public string pathConfig { get { return _pathConfig; } set { if (_pathConfig != value) { _pathConfig = value; } } }
        //public static List<PhidgetItem> ListofPhidgetItems = new List<PhidgetItem>();


        int AnalogUpDownLeftRight = AIO.Default.ph888_3_joy;

        public InterfaceKit Ph888_3_Analog_Cursors;

        MotorGUI mGUI { get { return Sys.SelectedMotor; } }
        GroupGUI gGUI { get { return Sys.SelectedGroup; } }
        CuesMotorGUI cmGUI { get { return Sys.SelectedCuesMotorGUIMotor; } }

        TextBlock SPtb, SVtb, MVtb;


        #region Attaching

        //Constructor
        PhidgetsInterfaces()
        {
            manager.Attach += new AttachEventHandler(m_Attach);
            manager.open();
        }
        //Flasher flasher = new Flasher();
        //MANAGER 
        void m_Attach(object sender, AttachEventArgs e)
        {
            if (typeof(InterfaceKit) == e.Device.GetType())
            {
                tempIfKit = new InterfaceKit();
                tempIfKit.Attach += new AttachEventHandler(ifKit_Attach);
                tempIfKit.Detach += new DetachEventHandler(ifKit_Detach);
                tempIfKit.Error += new ErrorEventHandler(ifKit_Error);
                tempIfKit.InputChange += new InputChangeEventHandler(ifKit_InputChange);
                tempIfKit.SensorChange += new SensorChangeEventHandler(ifKit_SensorChange);
                tempIfKit.open();
            }

            //bool init=Flasher.Flash(true, Flasher.FlashType.Normal); //todo razresiti ovu glupost darko20101209
            //Flasher
            if (typeof(RFID) == e.Device.GetType())
            {
                rfid = new RFID();
                rfid.Attach += new AttachEventHandler(rfid_Attach);
                rfid.Detach += new DetachEventHandler(rfid_Detach);
                rfid.Error += new ErrorEventHandler(rfid_Error);
                rfid.Tag += new TagEventHandler(rfid_Tag);
                //rfid.Tag += new TagEventHandler(rfid_Tag);
                rfid.TagLost += new TagEventHandler(rfid_TagLost);
                rfid.open();
            }

        }

        //void rfid_Tag(object sender, TagEventArgs e)
        //{
        //    RfidItem rfiditem = rfidList.Find(qq => qq.RfidTag == e.Tag);
        //    if (rfiditem != null)
        //    {

        //        MainWindow.mainWindowDispacher.Invoke
        //            (DispatcherPriority.Normal, (Action)delegate()
        //            {
        //                mainWindowEnabled.isAppEnabled = true;
        //                string s = string.Format("User login - {0}.", rfiditem.Name);
        //                Log.Write(s + string.Format(" rfid={0}", rfiditem.RfidTag));
        //                AlertLogic.Add(s);
        //            });
        //    }
        //    else
        //        MainWindow.mainWindowDispacher.Invoke(DispatcherPriority.Normal, (Action)delegate() { mainWindowEnabled.isAppEnabled = false; });
        //}
        void rfid_Tag(object sender, TagEventArgs e)
        {
            MainWindow.mainWindowDispacher.Invoke(DispatcherPriority.Normal, (Action)delegate ()
                     {
                         RfidItem rfiditem = Sys.rfidList.Find(qq => qq.RfidTag == e.Tag);
                         if (rfiditem != null)  
                        {
                             mainWindowEnabled.isAppEnabled = true;
                             string s = string.Format("User login - {0}.", rfiditem.Name);
                             Log.Write(s + string.Format(" rfid={0}", rfiditem.RfidTag), EventLogEntryType.Information);
                             AlertLogic.Add(s);

                         }
                         else
                             //mainWindowEnabled.isAppEnabled = false;
                             mainWindowEnabled.isAppEnabled = true;
                     });
        }


        //2013 darko test, remove!!!
        //void rfid_TagTEST(object sender, TagEventArgs e)
        //{
        //    mainWindowEnabled.isAppEnabled = true;
        //}


        void rfid_TagLost(object sender, TagEventArgs e)
        {
            MainWindow.mainWindowDispacher.Invoke(DispatcherPriority.Normal, (Action)delegate ()
            {
                RfidItem rfiditem = Sys.rfidList.Find(qq => qq.RfidTag == e.Tag);

                if (mainWindowEnabled.isAppEnabled != false) 
                {
                    string s = string.Format("User logout - {0}.", rfiditem.Name);
                   // mainWindowEnabled.isAppEnabled = false;
                    mainWindowEnabled.isAppEnabled = true;
                    List<MotorLogic> MovingMotorsList = Sys.StaticMotorList.FindAll(t => t.Can == true && t.Brake == false);
                    if (MovingMotorsList.Count > 0)
                    {
                        s += " Stopping motors.";

                        foreach (MotorLogic motor in MovingMotorsList)
                        {
                            UdpTx.UniqueInstance.SendPDO(motor.MotorID, CanSett.CanCommandStop, 0, 8);
                            UdpTx.UniqueInstance.SendPDO(motor.MotorID, CanSett.CanCommandIdle, 50, 8);
                            s += string.Format(" Motor={0}, ID={1}, CP={2}, CV={3};", motor.Title, motor.MotorID, motor.CP, motor.CV);
                        }
                    }
                    Log.Write(s + string.Format(" rfid={0}", rfiditem.RfidTag), EventLogEntryType.Warning);
                    AlertLogic.Add(s);
                }
            });
        }
        //void rfid_TagLost(object sender, TagEventArgs e)
        //{
        //    RfidItem rfiditem = rfidList.Find(qq => qq.RfidTag == e.Tag);

        //    if (mainWindowEnabled.isAppEnabled != false)
        //    {
        //        string s = string.Format("User logout - {0}.", rfiditem.Name);
        //        mainWindowEnabled.isAppEnabled = false;
        //        List<MotorLogic> MovingMotorsList = Sys.StaticMotorList.FindAll(t => t.Can == true && t.Brake == false);
        //        if (MovingMotorsList.Count > 0)
        //        {
        //            s += " Stopping motors.";

        //            foreach (MotorLogic motor in MovingMotorsList)
        //            {
        //                UdpTx.UniqueInstance.SendPDO(motor.MotorID, CanSett.CanCommandStop, 0, 2);
        //                //UdpTx.UniqueInstance.SendPDOSBC((byte)motor.CanChannel, (byte)motor.SecBrkControlAddress, 0);
        //                UdpTx.UniqueInstance.SendPDO(motor.MotorID, CanSett.CanCommandIdle, 50, 2);
        //                s += string.Format(" Motor={0}, ID={1}, CP={2}, CV={3};", motor.Title, motor.MotorID, motor.CP, motor.CV);
        //            }
        //        }
        //        MainWindow.mainWindowDispacher.Invoke(DispatcherPriority.Normal, (Action)delegate() { Log.Write(s + string.Format(" rfid={0}", rfiditem.RfidTag)); AlertLogic.Add(s); });
        //    }
        //}

        //ADDING INTERFACE KIT
        void ifKit_Attach(object sender, AttachEventArgs e)
        {
            //tempPhidgetItem = new PhidgetItem();
            //tempPhidgetItem.ifkit = (InterfaceKit)sender;
            InterfaceKit tttt = sender as InterfaceKit;

            if (tttt.SerialNumber == AnalogUpDownLeftRight)
            {
                Ph888_3_Analog_Cursors = tttt;
                Ph888_3_Analog_Cursors.outputs[AIO.Default.SelectGroup_888] = true;
            }

            InterfaceKitList.Add(tttt);

            //JOYSTICKS
            if (tttt.sensors.Count > 0)
                foreach (StJoystick joystick in Sys.joystickList) joystick.SetJoystickDirection();
        }

        void rfid_Attach(object sender, AttachEventArgs e)
        {
            rfid.outputs[0] = false;
            rfid.outputs[1] = false;
            rfid.Antenna = true;
            string s = "";
            foreach (var t in Sys.rfidList) s += string.Format("{0} {1};", t.Name, t.RfidTag);
            Log.Write("PhRfid xml read: " + s, EventLogEntryType.Information);
        }

        #endregion

        void ifKit_SensorChange(object sender, SensorChangeEventArgs e)
        {

            //20140512radovic
            //if (e.Index == 2) MainWindow.mainWindowDispacher.BeginInvoke(DispatcherPriority.Normal, (Action)delegate() { Sys.vagoniRxTx.SvRotInner = (int)(e.Value / 10); });
            //if (e.Index == 3) MainWindow.mainWindowDispacher.BeginInvoke(DispatcherPriority.Normal, (Action)delegate() { Sys.vagoniRxTx.SvRotOuter = (int)(e.Value / 10); });

            //deo za inicijalizaciju 
            // na pocetku broji sve analogne ulaze da ne bi pucao mora da se proveri da li je raw ulaz razlicit od nule -raw nedostupan !!!!

            //sensor change
            StJoystick joystick = Sys.joystickList.Find(q => q.JIndex == e.Index);

            if (joystick == null || Sys.StaticGroupList.Count - 1 < e.Index) return;// wth???? 20110220


            //Debug.WriteLine(string.Format("raw,norm={0},{1}", e.Value, StJoystick.JoyNormalized(e.Value)));


            //phJoystickItem = JoystickPhidget.phJoysticks.Find(qq => qq.JIndex == e.Index);
            //Console.WriteLine(string.Format("{0} joyval {1}  mvpos={2}/{4} mvneg={3}/{5}", DateTime.Now.ToLongTimeString(), e.Value, e.Value * PhdJoystickItem.k + PhdJoystickItem.n1, e.Value * PhdJoystickItem.k + PhdJoystickItem.n2, PhdJoystickItem.JoyNormalized(e.Value), PhdJoystickItem.JoyNormalized(e.Value)));
            //if (phJoystickItem == null) return;
            GroupLogic group = Sys.StaticGroupList[joystick.Group];
            if (group == null) return;//skinut uslov da se joy obradjuje samo kad je pritisnuto dugme
            motorsList = group.MotorsInGroup;
            if (motorsList.Count == 0) return;
            //if (!Sys.StaticGroupList[phJoystickItem.Group].Trip || !Sys.StaticGroupList[phJoystickItem.Group].Local || Sys.StaticGroupList[phJoystickItem.Group].Can)
            // todo18062010 trip checked by group logic?????? - pa ako ovo ne uradis joystick ce pokusati da pokrene grupu


            if (mainWindowEnabled.isAppEnabled == false)
            {
                joystick.MV = 0;
                joystick.Direction = CanSett.CanCommandStop;
                foreach (var moto in Sys.StaticMotorList) moto.MV = 0;
                return;
            }

            ////oreginal
            //if (e.Value < StJoystick.JMeasuredMid - StJoystick.JDeadZone)
            //{
            //    joystick.MV = joystick.JoyNormalized(e.Value);
            //    joystick.Direction = joystick.NegDirection;

            //}
            //else if (e.Value > StJoystick.JMeasuredMid + StJoystick.JDeadZone)
            //{
            //    //joystick.MV = StJoystick.JoyNormalized(e.Value);
            //    joystick.MV = joystick.JoyNormalized(e.Value);
            //    joystick.Direction = joystick.PosDirection;
            //
            //else
            //{
            //    joystick.MV = 0;
            //    joystick.Direction = CanSett.CanCommandStop;
            //}


            foreach (var moto in motorsList)
            {
                double vmax = group.IsSync ? motorsList.Min(sdf => sdf.MaxV) : moto.MaxV;
                joystick.JoyNormalized3(e.Value, moto, vmax, joystick);
            }

            //\\    Debug.WriteLine(string.Format("raw={0}, joy.mv={1:0.000}, direction={2}, lo/hi={3}/{4}, button={5} btnChng={6}", e.Value, joystick.MV, joystick.Direction, StJoystick.JMeasuredMid - StJoystick.JDeadZone, StJoystick.JMeasuredMid + StJoystick.JDeadZone, joystick.isButtonPressed, joystick.BtnChanged));

        }

        object objekt;
        //INPUT
        void ifKit_InputChange(object sender, InputChangeEventArgs e)
        {
          //  if (mainWindowEnabled.isAppEnabled == false) return;

            tempIfKit = sender as InterfaceKit;
            //Debug.WriteLine(DateTime.Now.ToLongTimeString() + " " + tempIfKit.SerialNumber.ToString() + " index=" + e.Index + " value=" + e.Value);
            motorsList = null;
            if (mGUI != null)
            {
                motorsList = mGUI._motor.toList();
                SPtb = mGUI.SP;
                SVtb = mGUI.SV;
                MVtb = mGUI.MV;
                objekt = mGUI._motor;
            }
            if (gGUI != null)
            {
                motorsList = gGUI._group.MotorsInGroup;
                SPtb = gGUI.SP;
                SVtb = gGUI.SV;
                MVtb = gGUI.MV;
                objekt = gGUI._group;
            }
            if (cmGUI != null)
            {
                motorsList = cmGUI.motor.toList();
                SPtb = cmGUI.SP;
                SVtb = cmGUI.SV;
                objekt = cmGUI.motor;
            }


            //deo za joystick
            if (tempIfKit.SerialNumber == AnalogUpDownLeftRight && Sys.StaticGroupList.Count >= 0) //Joystick          
                phANALOG(e.Index, e.Value);

            //if (tempIfKit.SerialNumber == PhidgetCue && (CueLogic.UniqueInstance.TempCueItems.Count > 0
            //    || e.Index == 2 || e.Index == 5 || e.Index == 6 || e.Index == 7 || e.Index == 10 || e.Index == 11 || e.Index == 12 || e.Index == 13 || e.Index == 14 || e.Index == 15) && e.Value) //15 je Prebaci na motor
            //    phCUE(e.Index);


            /* //////////////////////////////2013 remove todo
            if (tempIfKit.SerialNumber == PhidgetCue && e.Value) //15 je Prebaci na motor
                phCUE(e.Index);

            if ((motorsList != null && (mGUI != null || gGUI != null || cmGUI != null)) || (e.Index == 10 || e.Index == 11 || e.Index == 12 || e.Index == 13 || e.Index == 14 || e.Index == 15) && e.Value)
            {
                if (tempIfKit.SerialNumber == PhidgetMotorAndPreviews)
                    MotorsAndPreviews(motorsList, e.Index, e.Value, objekt);
            }


            if (tempIfKit.SerialNumber == PhidgetNumbers && e.Value)
                Numbers(e.Index);
            */
        }

        #region ERRORS
        //ERROR
        void ifKit_Error(object sender, ErrorEventArgs e)
        {
            InterfaceKit tIfKit = sender as InterfaceKit;//todo puca ovde cesto!!!!!
            //MainWindow.mainWindowDispacher.Invoke
            //(DispatcherPriority.Normal, (Action)delegate()
            {
                Console.WriteLine(string.Format("PHIDGET ERROR: {0}, {1}, {2} - {3} {4} {5}", tIfKit.ID, tIfKit.Name, DateTime.Now.ToLongTimeString(), e.Description, e.Code, e.exception.Code, e.exception.Data, e.exception.Description, e.exception.Message));
            }

            //);
        }

        //DETACH
        void ifKit_Detach(object sender, DetachEventArgs e)
        {
            InterfaceKit ik = sender as InterfaceKit;
            if (ik == null) return;
            ik.close();
            InterfaceKitList.Remove(ik);

            //tempPhidgetItem = ListofPhidgetItems.Find(qq => qq.ifkit.SerialNumber == ((InterfaceKit)sender).SerialNumber);
            //if (tempPhidgetItem != null)
            //{
            //    tempPhidgetItem.ifkit.close();
            //    ListofPhidgetItems.Remove(tempPhidgetItem);
            //}

        }

        //ERROR
        void rfid_Error(object sender, ErrorEventArgs e)
        {
            throw new NotImplementedException();
        }



        //DETACH
        void rfid_Detach(object sender, DetachEventArgs e)
        {
            MessageBox.Show(e.Device.Name);
        }
        #endregion

        #region Commands
        //B
        void MotorsAndPreviews(List<MotorLogic> ml, int index, bool value, object objekt)
        {
            if (value == false) return;
            switch (index)
            {
                case 0:
                    commands.AutoStarting(ml, objekt);
                    break;
                case 1:

                    commands.Stop(ml);
                    break;
                case 2:
                    MainWindow.mainWindowDispacher.BeginInvoke(DispatcherPriority.Normal, (Action)delegate ()
                   {
                       if (mGUI != null)
                       {
                           if (ml[0].GroupNo == -1)
                               commands.ManualStarting(ml, objekt); //RELEASE
                       }
                       else if (gGUI != null)
                       {
                           commands.ManualStarting(ml, objekt); //RELEASE
                       }
                   });
                    break;
                case 3:
                    MainWindow.mainWindowDispacher.BeginInvoke(DispatcherPriority.Normal, (Action)delegate ()
                   {
                       commands.SP_Popup(ml, SPtb, false);
                   });
                    break;
                case 4:
                    MainWindow.mainWindowDispacher.BeginInvoke(DispatcherPriority.Normal, (Action)delegate ()
                   {
                       //commands.SVMV_Popup(ml, SVtb, false);
                   });
                    break;
                case 5:

                    MainWindow.mainWindowDispacher.BeginInvoke(DispatcherPriority.Normal, (Action)delegate ()
                   {
                       //commands.SVMV_Popup(ml, MVtb, false);
                   });
                    break;
                case 6:
                    MainWindow.mainWindowDispacher.BeginInvoke(DispatcherPriority.Normal, (Action)delegate ()
                    {
                        if (mGUI != null)
                        {
                            int number;
                            double d;
                            TextBlock tb = new TextBlock();
                            tb.Name = "Group"; // promene 16 jun : obavestava popup ko je
                            commands.PopupCommand(tb, false);
                            if (double.TryParse(tb.Text, out d))
                                number = Convert.ToInt32(d);
                            else
                                return;
                            mGUI._motor.GroupNo = number;
                        }
                        if (gGUI != null)
                        {
                            commands.ResetGroup(gGUI);
                        }

                    });

                    break;
                case 7:
                    if (cmGUI != null)
                        cmGUI.motor.Cued = !cmGUI.motor.Cued;
                    if (mGUI != null && value)
                        mGUI._motor.Cued = !mGUI._motor.Cued;
                    break;
                case 8:
                    if (mGUI != null)
                        commands.resetTrip(mGUI._motor.toList());
                    if (gGUI != null)
                        commands.resetTrip(gGUI._group.MotorsInGroup);
                    break;
                case 9:
                    if (gGUI != null)
                        gGUI._group.IsSync = !gGUI._group.IsSync;
                    break;

                case 10:
                    VisibilityStatesLogic.UniqueInstance.ShowAllShowNone(true);
                    break;
                case 11:
                    VisibilityStatesLogic.UniqueInstance.ShowAllShowNone(false);
                    break;
                case 12:
                    VisibilityStatesLogic.UniqueInstance.ShowCuedMotors();
                    break;
                case 13:
                    VisibilityStatesLogic.UniqueInstance.ShowMovingMotors();
                    break;
                case 14:
                    VisibilityStatesLogic.UniqueInstance.ShowTrippedMotors();
                    break;
                case 15:
                    commands.StopAllMotors();
                    break;
            }
        }

        //D
        //20140406 todo kandidat za brisanje
        void phANALOG(int index, bool value)
        {
            StJoystick joyTemp;
            List<MotorLogic> motorsList;

            switch (index)
            {
                //case 0:
                //    if (value)
                //    {
                //        MainWindow.mainWindowDispacher.BeginInvoke(DispatcherPriority.Normal, (Action)delegate ()
                //        {
                //            //if (Sys.StaticMotorList.All(qq => qq.Brake))
                //            //{
                //            Sys.GroupListBox.Focus();

                //            if (Sys.GroupListBox.SelectedIndex == -1)
                //                Sys.GroupListBox.SelectedIndex = 0;
                //            //}
                //            //else
                //            //    AlertLogic.Add("Selection Change Not Allowed: Brakes Released ");
                //        });
                //    }
                //    break;
                case 0:
                    //joyTemp = Sys.joystickList.Find(q => q.JIndex == 0); if (!(joyTemp == null)) joyTemp.isButtonPressed = value;
                    //motorsList = Sys.StaticGroupList[0].MotorsInGroup; if (motorsList.Count > 0) foreach (MotorLogic ml in motorsList) ml.iMv.BtnChanged = joyTemp.ButtonChanged; //todo cleanup
                    break;
                case 1:
                    //joyTemp = Sys.joystickList.Find(q => q.JIndex == 1); if (!(joyTemp == null)) joyTemp.isButtonPressed = value;
                    //motorsList = Sys.StaticGroupList[1].MotorsInGroup; if (motorsList.Count > 0) foreach (MotorLogic ml in motorsList) ml.iMv.BtnChanged = joyTemp.ButtonChanged;
                    break;
                /* //sklonjeno 20140406 - ne koristi se
           case 1: ProcessJoystick(value, 0); break;
           case 2: ProcessJoystick(value, 1); break;
           case 3: ProcessJoystick(value, 2); break;
                 * */
                case 4: // UP - upcommand and change selection
                    if (mGUI == null && gGUI == null) break;

                    //cursor down only if all brakes locked!
                    if ((mGUI != null && mGUI._motor.Brake == true) || (gGUI != null && gGUI._group.Brake == true) || (gGUI != null && gGUI._group.Brake == false && gGUI._group.MotorsInGroup.Count == 0))
                    {
                        if (value) System.Windows.Forms.SendKeys.SendWait("{Up}");
                        break;
                    }

                    //if motor is dead dont do nothing
                    if ((mGUI != null && mGUI._motor.Can == false) || (gGUI != null && gGUI._group.Can == false)) break;

                    //if group is empty do nothing
                    if (gGUI != null && gGUI._group.MotorsInGroup.Count == 0) break;

                    if (mGUI != null)
                    {
                        if (value)
                            commands.Up_PreviewMouseLeftButtonDown(mGUI._motor.toList());
                        else
                            commands.Up_PreviewMouseLeftButtonUp(mGUI._motor.toList());
                    }
                    else if (gGUI != null)
                    {
                        if (value)
                            commands.Up_PreviewMouseLeftButtonDown(gGUI._group.MotorsInGroup);
                        else
                            commands.Up_PreviewMouseLeftButtonUp(gGUI._group.MotorsInGroup);
                    }
                    break;

                case 5: // DOWN - downcommand and change selection
                    if (mGUI == null && gGUI == null) break;

                    //cursor down only if all brakes locked!
                    if ((mGUI != null && mGUI._motor.Brake == true) || (gGUI != null && gGUI._group.Brake == true) || (gGUI != null && gGUI._group.Brake == false && gGUI._group.MotorsInGroup.Count == 0))//todo da lije ovaj count==0 dobar 20110220
                    {
                        if (value) System.Windows.Forms.SendKeys.SendWait("{Down}");
                        break;
                    }

                    //if motor is dead dont do nothing
                    if ((mGUI != null && mGUI._motor.Can == false) || (gGUI != null && gGUI._group.Can == false)) break;

                    //if group is empty do nothing
                    if (gGUI != null && gGUI._group.MotorsInGroup.Count == 0) break;
                    ////if group is moving do nothing //todo 20110225 ovo sam stavio sa idejom da sprecim da ako je motor/grupa u auto da se pritiskom na goredole prebaCI NA MAN, ali ovo resenje ima bocne vetrove pa ostavljam za kasnije
                    //if (gGUI != null && gGUI._group.Moving == true) break;

                    //everything is fine
                    if (mGUI != null)
                    {
                        if (value)
                            commands.Down_PreviewMouseLeftButtonDown(mGUI._motor.toList());
                        else
                            commands.Down_PreviewMouseLeftButtonUp(mGUI._motor.toList());
                    }
                    else
                    {
                        if (value)
                            commands.Down_PreviewMouseLeftButtonDown(gGUI._group.MotorsInGroup);
                        else
                            commands.Down_PreviewMouseLeftButtonUp(gGUI._group.MotorsInGroup);
                    }
                    break;
                case 6:
                    if (value)
                        System.Windows.Forms.SendKeys.SendWait("{Left}");
                    break;
                case 7:
                    if (value)
                        System.Windows.Forms.SendKeys.SendWait("{Right}");
                    break;

            }
        }

        /* //sklonjeno 20140406 - ne koristi se posto vise nema dugmeta na dzoj
        void ProcessJoystick(bool joystickButtonPressed, int JoystickIndex)
        {
            StJoystick joystick = joystickList.Find(q => q.JIndex == JoystickIndex);
            if (joystick == null) return;
            if (Sys.StaticGroupList.Count - 1 < joystick.Group) return;
            joystick.isButtonPressed = joystickButtonPressed;
            List<MotorLogic> motorsList = Sys.StaticGroupList[joystick.Group].MotorsInGroup;

            if (motorsList.Count == 0) return;

            bool IsBrakeOn = motorsList.All(qq => qq.Brake);

            foreach (MotorLogic motor in motorsList)
            {
                if (joystickButtonPressed)
                {
                    if (IsBrakeOn)
                        commands.ManualStarting(motor.toList(), joystick);
                }
                else
                    if (motor.state != states.Error && motor.state != states.Stopping1 && motor.state != states.Stopped && motor.state != states.Idle && motor.iMv == joystick && !motor.Brake && motor.state != states.Started)
                        commands.Stopping(motor);
            }

            if (!motorsList.All(qq => qq.Brake) && joystickButtonPressed)
                MainWindow.mainWindowDispacher.Invoke(DispatcherPriority.Normal, (Action)delegate() { { AlertLogic.Add(joystick.Group.ToString() + ": brakes released - unable to move"); } });//todo mici humor

        }
        */


        public void CheckKey()
        {//ovaj metod radi ako postoji obican kljuc a ne smartkey
            InterfaceKit ik = InterfaceKitList.Find(qq => qq.SerialNumber == AnalogUpDownLeftRight);
            if (ik == null) return;
            if (ik.inputs[3] == false)
            {
                mainWindowEnabled.isAppEnabled = true;

                MainWindow.mainWindowDispacher.Invoke(DispatcherPriority.Normal, (Action)delegate ()
                {
                    {
                        MessageBoxResult result = MessageBox.Show("Console OFF - please turn key", "TURN OFF", MessageBoxButton.OK);

                        switch (result)
                        {
                            case MessageBoxResult.OK:
                                App.Current.Shutdown();
                                break;

                        }
                    }
                });
            }
            else
                mainWindowEnabled.isAppEnabled = true;
        }
        #endregion

        #region OUTPUTS

        //PreviewButtons activePreview = PreviewButtons.All;
        //PanelTabs activePanel = PanelTabs.Motors;
        //enum PreviewButtons { All, None, CueDelegateStates, Moving, Tripper }
       // public enum PanelTabs { Motors = 0, Cues = 1, Diag = 2 }
        //public void SetActivePanel(PanelTabs pb)        {            activePanel = pb;        }

        public void AllOutsOff()
        {
            foreach (InterfaceKit t in InterfaceKitList)
            {
                if (t == null) continue;
                for (int i = 0; i < t.outputs.Count; i++)
                    t.outputs[i] = false;
            }
        }
        #endregion

        #region UniqueInstance
        class PhidgetsInterfacesCreator
        {
            static PhidgetsInterfacesCreator() { }
            internal static readonly PhidgetsInterfaces uniqueInstance = new PhidgetsInterfaces();
        }
        public static PhidgetsInterfaces UniqueInstance
        {
            get { return PhidgetsInterfacesCreator.uniqueInstance; }
        }

        public void initPhidget() { }

        #endregion
        #region INotifyPropertyChanged Members //todo 20130110 da li je ovo potrebno
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnNotify(String parameter)
        {
            if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(parameter));
        }
        #endregion
    }

    public class Flasher
    {
        static DispatcherTimer dt = new DispatcherTimer();

        public Flasher()
        {
            dt.Interval = TimeSpan.FromMilliseconds(100);
            dt.Tick += new EventHandler(dt_Tick);
            dt.Start();
        }

        static void dt_Tick(object sender, EventArgs e)
        {
            IncrementCounter();
        }

        public enum FlashType { VerySlow = 20, Slow = 15, Normal = 10, Fast = 6, VeryFast = 3 }
        static int counter;
        public static void IncrementCounter() { counter++; }

        public static bool Flash(bool SignalEnabled, FlashType type)
        {
            return (SignalEnabled == false ? false : counter % (int)type == 0);
        }
        public static bool FlashAlwaysOn(bool SignalEnabled, FlashType type)
        {
            return (SignalEnabled == false ? true : counter % (int)type == 0);
        }

    }

    public class StJoystick : IMv
    {
        int _group = 0, _jindex = 0, _deadZone = 0, posdirection, negdirection, _direction = Properties.CanSettings.Default.CanCommandStop;
        bool _isInverted = false;
        double _mv;
        public bool BtnChanged { get; set; }

        Properties.Settings Sett = Properties.Settings.Default;
        Properties.CanSettings CanSett = Properties.CanSettings.Default;

        [XmlAttribute("Group")]
        public int Group { get { return _group; } set { _group = value; } }

        [XmlAttribute("JIndex")]
        public int JIndex { get { return _jindex; } set { _jindex = value; } }

        //[XmlAttribute("DeadZone")]
        //public int DeadZone { get { return _deadZone; } set { _deadZone = value; } }

        [XmlAttribute("IsInverted")]
        public bool IsInverted { get { return _isInverted; } set { _isInverted = value; } }
        //bool _isButtonPressed;
        //[XmlIgnore]
        //public bool isButtonPressed { get { return _isButtonPressed; } set { _ButtonChanged = _isButtonPressed != value; _isButtonPressed = value; } }
        //[XmlIgnore]
        //bool _ButtonChanged;
        //public bool ButtonChanged { get { return _ButtonChanged; } }

        [XmlIgnore]
        public int PosDirection { get { return posdirection; } set { posdirection = value; } }
        [XmlIgnore]
        public int NegDirection { get { return negdirection; } set { negdirection = value; } }

        [XmlIgnore]
        //public static double JMeasuredNeg = 160;//20140522 todo staviti ovo u config JoystickMeasuredNeg
        public static double JMeasuredNeg = Settings.Default.JoystickMeasuredNeg;//20140522 todo staviti ovo u config JoystickMeasuredNeg
        [XmlIgnore]
        //public static double JMeasuredPos = 840;
        public static double JMeasuredPos = Settings.Default.JoystickMeasuredPos;
        [XmlIgnore]
        //public static double JMeasuredMid = 500;
        public static double JMeasuredMid = Settings.Default.JoystickMeasuredMid;
        [XmlIgnore]
        public static double JDeadZone = Settings.Default.JoystickDeadZone;
        //[XmlIgnore]
        //public static int n1 = -100;
        //[XmlIgnore]
        //public static double n2 = -100F * (500 + Settings.Default.JoystickDeadZone) / (500 - Settings.Default.JoystickDeadZone);
        //[XmlIgnore]
        ////public static int low = 500 - Settings.Default.JoystickDeadZone;
        //public static int low = 500 - Convert.ToInt32(JDeadZone);//todo 20131006 jdeadzone=0 ako se inicijalizuje posle low, tj zavisi od redosleda operacija. PROMENI TODO
        //[XmlIgnore]
        ////public static int hi = 500 + Settings.Default.JoystickDeadZone;
        //public static int hi = 500 + Convert.ToInt32(JDeadZone);
        //[XmlIgnore]
        //public static double k = (100F / (500 - Settings.Default.JoystickDeadZone));



        [XmlIgnore]
        public static double x1Neg = JMeasuredNeg;
        [XmlIgnore]
        public static double x2Neg = JMeasuredMid - JDeadZone;
        [XmlIgnore]
        public double y1Neg = 100;
        [XmlIgnore]
        public static double y2Neg = 0;
        [XmlIgnore]
        public static double x1Pos = JMeasuredMid + JDeadZone;
        [XmlIgnore]
        public static double x2Pos = JMeasuredPos;
        [XmlIgnore]
        public static double y1Pos = 0;
        [XmlIgnore]
        public double y2Pos = 100;
        [XmlIgnore]
        public double kNeg { get { return (y2Neg - y1Neg) / (x2Neg - x1Neg); } }
        [XmlIgnore]
        public double nNeg { get { return y2Neg - (kNeg * x2Neg); } }
        [XmlIgnore]
        public double kPos { get { return -kNeg; } }
        [XmlIgnore]
        public double nPos { get { return y2Pos - (kPos * x2Pos); } }

        public void SetJoystickMaxV(double _maxV) { y1Neg = y2Pos = _maxV; }

        public double JoyNormalized(double x)
        {
            if (x > JMeasuredMid + JDeadZone) return _mv = J2(x, kPos, nPos);
            else if (x < JMeasuredMid - JDeadZone) return _mv = J2(x, kNeg, nNeg);
            else return _mv = 0;
        }

        public double JoyNormalized3(double Win, MotorLogic moto, double _vmax, StJoystick joystick)
        {
            //double Win2 = Math.Abs(Win - JMeasuredMid);
            double Vmax = _vmax;
            double k = (Vmax - 0) / (JMeasuredPos - (JMeasuredMid + JDeadZone));
            double nPos = -k * (JMeasuredMid + JDeadZone);
            double nNeg = k * (JMeasuredMid - JDeadZone);
            double WinA = JMeasuredNeg;
            double WinB = JMeasuredMid - JDeadZone;
            double WinC = JMeasuredMid;
            double WinD = JMeasuredMid + JDeadZone;
            double WinE = JMeasuredPos;

            double Wout;

            if ((Win > WinB) && (Win < WinD))
            { Wout = 0; moto.iMv.Direction = CanSett.CanCommandStop; }
            else
                if (Win <= WinA)
            { Wout = Vmax; moto.iMv.Direction = joystick.NegDirection; }
            else
                    if (Win >= WinE)
            { Wout = Vmax; moto.iMv.Direction = joystick.PosDirection; }
            else
                        if (Win > WinA && Win <= WinB)
            { Wout = -k * Win + nNeg; moto.iMv.Direction = joystick.NegDirection; }
            else
                            if (Win < WinE && Win >= WinD)
            { Wout = k * Win + nPos; moto.iMv.Direction = joystick.PosDirection; }
            else
            { Wout = double.NaN; }

            //if (!joystick.isButtonPressed) Wout = 0;
            //if (!joystick.isButtonPressed) moto.iMv.Direction = CanSett.CanCommandStop;

            return moto.iMv.MV = Wout;
        }

        //public double JoyNormalized(double x)
        //{
        //    if (x > JMeasuredMid + JDeadZone) return J2(x, kPos, nPos);
        //    else if (x < JMeasuredMid - JDeadZone) return J2(x, kNeg, nNeg);
        //    else return 0;
        //}
        //public static double JoyNormalizedOLD(double x)
        //{
        //    if (x > JMeasuredMid) return J2(x, kPos, nPos);
        //    return J2(x, kNeg, nNeg);
        //}
        public double J2(double x, double k, double n)
        {
            double tempValue = Math.Abs(k * x + n);

            if (tempValue > 98) tempValue = 100;
            if (tempValue < 2) tempValue = 0;
            //Debug.WriteLine("tempval=" + tempValue);
            return tempValue;
        }
        //public static double JNeg(double x) { return kNeg * x + nNeg; }


        [XmlIgnore]
        public int Direction { get { return _direction; } set { _direction = value; } }
        [XmlIgnore]
        public double MV { get { return _mv; } set { _mv = value; } }


        public void SetJoystickDirection()
        {
            PosDirection = (IsInverted ? CanSett.CanCommandManNegative : CanSett.CanCommandManPositive);
            NegDirection = (IsInverted ? CanSett.CanCommandManPositive : CanSett.CanCommandManNegative);
        }

    }

}








