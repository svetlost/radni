﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using vxlapi_NET20;
using LogAlertHB;
using System.Diagnostics;

namespace Server
{
    public class VectorConfig : Configs
    {

        #region GLOBALS


        private static uint busType = (uint)XLClass.XLbusTypes.XL_BUS_TYPE_CAN;
        public static int portHandle = -1;
        public static int eventHandle = -1;
        static UInt64 accessMask = 0;
        private static UInt64 permissionMask;
        private static String appName = "Server";
        static XLClass.xl_driver_config driverConfig = new XLClass.xl_driver_config();
        public static XLDriver App = new XLDriver();
        XLClass.XLstatus xlStatus = XLClass.XLstatus.XL_SUCCESS;
        public static UInt64 sendSyncAccessMask = 0;




        private static uint hwType = 0;
        private static uint hwIndex = 0;
        private static uint hwChannel = 0;
        //private static uint busType = (uint)XLClass.XLbusTypes.XL_BUS_TYPE_CAN;
        private static UInt64 txMask = 0;
        private static uint flags = 0;
        string s;

        #endregion

        public VectorConfig()
        {
            try
            {
                s = "Vector Can Init \n";

                foreach (CanChannelConfiguration ch in CanChannelConfigurationList)
                {
                    if (ch.Enabled == true)
                    {
                        accessMask = accessMask | (ulong)(1 << ch.Channel);
                        if (ch.SendSync == true) sendSyncAccessMask = sendSyncAccessMask | (ulong)(1 << ch.Channel);
                    }
                }

                xlStatus = App.XL_OpenDriver();
                s += "\n   XL_OpenDriver:" + xlStatus.ToString();
                if (xlStatus != XLClass.XLstatus.XL_SUCCESS) return;

                xlStatus = App.XL_GetDriverConfig(ref driverConfig);
                s += "\n   XL_GetDriverConfig:" + xlStatus.ToString();
                if (xlStatus != XLClass.XLstatus.XL_SUCCESS) return;

                permissionMask = accessMask;
                xlStatus = App.XL_OpenPort(ref portHandle, appName, accessMask, ref permissionMask, 1, busType);
                s += "\n   XL_OpenPort:" + xlStatus.ToString() + ", AccessMask=" + accessMask.ToString() + ", PermissionMask=" + permissionMask.ToString();
                if (xlStatus != XLClass.XLstatus.XL_SUCCESS) return;

                xlStatus = App.XL_SetNotification(portHandle, ref eventHandle, Properties.Settings.Default.SetNotificationQueueLevel);
                s += "\n   XL_SetNotification:" + xlStatus.ToString();
                if (xlStatus != XLClass.XLstatus.XL_SUCCESS) return;

                foreach (CanChannelConfiguration ch in CanChannelConfigurationList)
                {
                    if (ch.Enabled == true)
                    {
                        xlStatus = App.XL_CanSetChannelBitrate(portHandle, (ulong)(1 << ch.Channel), ch.BitRate);
                        s += "\n     XL_CanSetChannelBitrate[" + ch.Channel + "]=" + ch.BitRate.ToString() + ":" + xlStatus.ToString();
                        // if (xlStatus != XLClass.XLstatus.XL_SUCCESS) return;
                        xlStatus = App.XL_ActivateChannel(portHandle, (ulong)(1 << ch.Channel), (uint)XLClass.XLbusTypes.XL_BUS_TYPE_CAN, (uint)XLClass.XLChannelFlags.XL_ACTIVATE_NONE);
                        s += "\n     XL_ActivateChannel[" + ch.Channel + "]:" + xlStatus.ToString();
                        //if (xlStatus != XLClass.XLstatus.XL_SUCCESS) return;
                    }
                }
            }
            catch (Exception e1)
            {
                AlertLogic.Add("\nCAN initialization Error :" + e1.Message);
                Log.Write("CAN initialization Error :" + e1.Message, EventLogEntryType.Error);
                CanDriverInitialized = false;
            }

            AlertLogic.Add(s += "\nCAN initialized OK.");
            Log.Write(s, EventLogEntryType.Information);
            CanDriverInitialized = true;

            // StartCan();
        }

        public override void CloseCanDriver()
        {
            AlertLogic.Add("CAN driver closed.");
            XLClass.XLstatus s = App.XL_CloseDriver();
            Log.Write("Vector driver closing. Status=" + s, EventLogEntryType.Information);
        }



    }
}

